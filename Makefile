# Author: Lilian BESSON
# Email: lilian DOT besson AT normale D O T fr
# Version: 3
# Date: 12-12-2015
#
# Makefile for the info-mp-2015-2016 folder and git project.

CP = ~/bin/CP --delete --exclude=\.htaccess
GPG = gpg --no-batch --use-agent --detach-sign --armor --quiet --yes

git:
	git add README.md Makefile
	git commit -m "Auto commit made with 'make git'."
	git push

checkbuilds:
	ls -larth *s/solutions/.buildinfo *x/solutions/.buildinfo

send_zamok:
	cd /home/lilian/ ; $(CP) ./teach/info-mp-2015-2016/* besson@zamok.crans.org:~/www/infoMP/ ; cd /home/lilian/teach/info-mp-2015-2016/
	# cd /home/lilian/ ; $(CP) ./teach/info-mp-2015-2016/* besson@zamok.crans.org:~/www/teach/info-mp-2015-2016/ ; cd /home/lilian/teach/info-mp-2015-2016/

doc:
	cd TPs/   ; make clean html ; cd ..
	cd TDs/   ; make clean html ; cd ..
	cd DSs/   ; make clean html ; cd ..
	cd oraux/ ; make clean html ; cd ..

html:
	cd TPs/   ; make html ; cd ..
	cd TDs/   ; make html ; cd ..
	cd DSs/   ; make html ; cd ..
	cd oraux/ ; make html ; cd ..

clean.doc:
	cd TPs/   ; make clean ; cd ..
	cd TDs/   ; make clean ; cd ..
	cd DSs/   ; make clean ; cd ..
	cd oraux/ ; make clean ; cd ..

clean.pycache:
	-mv -vf *.pyc */*.pyc /tmp/
	-rm -vfr __pycache__/ */__pycache__/
	-rm -vf *.pyc */*.pyc

zipsmall:
	cd /home/lilian/teach/info-mp-2015-2016/ ; zip -y -r -9 ~/info-mp-2015-2016_small.zip ./*.md ./*.py ./*s/*.md ./*s/*.py ./*s/*.rst ./*.html ./Makefile ./*s/Makefile ./*s/*.png -x mails/*
	$(GPG) ~/info-mp-2015-2016_small.zip
	$(CP) ~/info-mp-2015-2016_small.zip* besson@zamok.crans.org:~/www/dl/
	# $(CP) ~/info-mp-2015-2016_small.zip* lbesson@ssh.dptinfo.ens-cachan.fr:~/www/dl/
	mv -vf ~/info-mp-2015-2016_small.zip* ~/Dropbox/

zip:
	cd /home/lilian/ ; zip -y -r -9 ~/info-mp-2015-2016.zip ./teach/info-mp-2015-2016/ ; cd /home/lilian/teach/info-mp-2015-2016/
	$(GPG) ~/info-mp-2015-2016.zip
	$(CP) ~/info-mp-2015-2016.zip* besson@zamok.crans.org:~/www/dl/
	# $(CP) ~/info-mp-2015-2016.zip* lbesson@ssh.dptinfo.ens-cachan.fr:~/www/dl/
	mv -vf ~/info-mp-2015-2016.zip* ~/Dropbox/
