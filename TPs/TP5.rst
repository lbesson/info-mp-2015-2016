`TP5 : Résolution numérique des équations différentielles <../TP5.pdf>`_
------------------------------------------------------------------------
On s'intéresse à la méthode d'Euler pour la résolution approchée du pendule pesant harmonique ou amorti, ainsi qu'à d'autres méthodes.

.. image:: TP5__euler_ordre2_vitesse.png
   :align: center
   :scale: 60%

.. image:: TP5__ptmilieu_vitesse.png
   :align: center
   :scale: 60%

Plusieurs portraits de phases, montrant les trois régimes possibles (sous-critique, critique, sur-critique) :


.. image:: TP5__15_portraits_de_phase.png
   :align: center
   :scale: 90%


Et une jolie animation d'un pendule pesant idéal en régime sous-critique :

.. image:: TP5__animationq6__sous-critique.gif
   :align: center
   :scale: 85%

--------------------------------------------------------------------------------

Documentation
^^^^^^^^^^^^^
.. automodule:: TP5
   :members:
   :undoc-members:

--------------------------------------------------------------------------------

Sortie du script
^^^^^^^^^^^^^^^^
.. runblock:: console

   $ python TP5.py

Le fichier Python se trouve ici : :download:`TP5.py`.
