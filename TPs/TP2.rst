`TP2 : Algorithmes et complexité <../TP2.pdf>`_
-----------------------------------------------
Effectifs cumulés
^^^^^^^^^^^^^^^^^
.. image:: TP2__Exemple_Effectifs.png
.. image:: TP2__Exemple_Effectifs_cumulés.png

--------------------------------------------------------------------------------

Documentation
^^^^^^^^^^^^^
.. automodule:: TP2
   :members:
   :undoc-members:
   :show-inheritance:

--------------------------------------------------------------------------------

Sortie du script
^^^^^^^^^^^^^^^^
.. runblock:: console

   $ python TP2.py

Le fichier Python se trouve ici : :download:`TP2.py`.
