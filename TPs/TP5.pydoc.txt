Help on module TP5:

NAME
    TP5 - TP5 au Lycée Lakanal (sujet rédigé par Arnaud Basson), sur le pendule pesant.

FILE
    /home/lilian/teach/info-mp-2015-2016/TPs/TP5.py

DESCRIPTION
    - *Date :* mercredi 13-01 et jeudi 14-01 (*2016*),
    - *Auteur :* Lilian Besson, pour le cours d'informatique pour tous en prépa MP (http://perso.crans.org/besson/infoMP/),
    - *Licence :* MIT Licence (http://lbesson.mit-license.org).

FUNCTIONS
    G(Y, t)
        Opérateur :math:`g: (Y, t) \mapsto g(Y, t)`, ici simplement égal à :math:`g(Y, t) = f(Y)` (donné en :func:`f`).
        
        
        En utilisant ``scipy.integrate.odeint`` (`voir sa doc <https://docs.scipy.org/doc/scipy/reference/generated/scipy.integrate.odeint.html>`_, issue du `module scipy.integrate <https://docs.scipy.org/doc/scipy/reference/integrate.html>`_), on obtient la courbe et le portrait de phase suivant :
        
        .. image:: TP5__scipy_integrate_odeint_position.png
           :align: center
           :scale: 80%
        
        .. image:: TP5__scipy_integrate_odeint_vitesse.png
           :align: center
           :scale: 80%
    
    animationq6(regime='critique', show=False)
        Calcule (et affiche si ``show=True``) une animation du mouvement d'un pendule pesant.
        
        - *Arguments :*
        
           - ``regime`` peut valoir ``"sous-critique"``, ``"critique"``,  ``"sur-critique"`` ou  ``"nul"`` (:math:`\dot{\theta_0} = 0` donc aucun mouvement).
           - ``show=False`` par défaut, mais l'animation est affichée avant d'être sauvegardée si ``True``.
        
        
        - Exemple de **régime sous-critique** (:math:`\dot{\theta_0} < \dot{\theta}_{\text{critique}}`), avec des oscillations perpétuelles, et un angle toujours borné (:math:`0 \leq \theta \leq \theta_{\max} < \pi/2`) et en dessous de l'horizontale :
        
        .. image:: TP5__animationq6__sous-critique.gif
           :align: center
           :scale: 85%
        
        
        - Exemple de **régime presque critique**, malgré le choix de :math:`\dot{\theta_0} = \dot{\theta}_{\text{critique}} = 2 \omega_0`, le pendule n'atteint pas l'équilibre instable :math:`\theta = \pi` (à la première oscillation on y croit presque, mais non) :
        
        .. image:: TP5__animationq6__critique.gif
           :align: center
           :scale: 85%
        
        
        - Exemple de **régime sur-critique** (:math:`\dot{\theta_0} > \dot{\theta}_{\text{critique}}`), avec des oscillations perpétuelles, toujours dans le même sens, :math:`0 \leq \theta \to +\infty` qui diverge (au-delà de :math:`2\pi`, ça compte les tours) :
        
        .. image:: TP5__animationq6__sur-critique.gif
           :align: center
           :scale: 85%
        
        
        - Exemple de régime *"nul"*, avec :math:`\theta_0 = 0` et :math:`\dot{\theta_0} = 0` , il n'y aucun mouvement (et c'est donc l'animation la plus excitante du jour...) :
        
        .. image:: TP5__animationq6__nul.gif
           :align: center
           :scale: 85%
    
    botafumeiro()
        Désormais, on considère le **pendule excité**.
        
        TODO !
        
        Et une vidéo montrant le Botafumeiro en action (via YouTube) :
        
        .. youtube:: https://www.youtube.com/watch?v=S_s2Rf0Z0eE
    
    euler(h, tmax, theta0, thetaPoint0)
        Résoud numériquement l'équation différentielle d'ordre 2 du pendule simple :eq:`pendule` par `la méthode d'Euler <https://fr.wikipedia.org/wiki/M%C3%A9thode_d%27Euler>`_ pour :math:`t \in [0, t_{\max}]`, avec un pas de temps :math:`h > 0`.
        
        .. math:: \frac{d^2 \theta}{d t^2}(t) + \frac{g}{l} \sin(\theta(t)) = 0.
           :label: pendule
        
        
        Pour utiliser un schéma d'Euler, il nous faut écrire cette équation (ordinaire d'ordre 2, mais *non linéaire !*) sous la forme :math:`Y'(t) = f(t, Y)`, où :math:`Y = [\theta, \dot\theta]` :
        
        .. math:: f(Y) = [Y_1, - (g/l) \sin(Y_0)]
        
        Et dès lors, le schéma de mise à jour d'Euler s'écrit :
        
        .. math:: Y_{i+1} = Y_i + (t_{i+1} - t_i) \times f(t_i, Y_i)
        
        Mais dans notre cas, les temps discrets :math:`t_i` sont *uniformément* espacés,
        donc :math:`\forall i \in \{0,\dots,n-1\},\; t_{i+1} - t_{i} = h` (le *pas de temps*), donc le schéma se simplifie en :eq:`schemaeuler` :
        
        .. math:: Y_{i+1} = Y_i + h \times f(t_i, Y_i)
           :label: schemaeuler
        
        
        - L'opérateur :math:`f: (t, Y) \mapsto f(t, Y)` est vectoriel (de taille :math:`2 \times 2`), et il est donné par la fonction :func:`f`.
        - Les conditions initiales sont données par ``theta0`` et ``thetaPoint0``.
        
        - Renvoit un tableau (``list``) contenant les valeurs de :math:`u_c` calculées aux instants :math:`t_i = i \times h` (pour :math:`0 \leq i \leq t_{\max} / h)`.
        
        - *Arguments* :
           - ``h`` est un pas de temps (:math:`h > 0`),
           - ``tmax`` est la durée totale de la simulation numérique (:math:`t_{\max} > 0`),
           - ``theta0``:math:`= \theta_0` est la valeur de :math:`\theta(t=0)`,
           - et ``thetaPoint0``:math:`= \dot{\theta_0}` est la valeur de :math:`\dot\theta(t=0) = \left(\frac{d\theta}{dt}\right)(t=0)`.
        
        - *Hypothèse* : On résoudra l'équation pour :math:`t \geq 0`.
        
        - Exemple (question I.2.c) :
        
        >>> theta0 = 0
        >>> thetaPoint0 = 6
        >>> tmax = 5
        >>> nbPoints = 2000  # On choisit 2000 points
        >>> h = tmax / nbPoints
        >>> thetas = euler(h, tmax, theta0, thetaPoint0)
        >>> np.shape(thetas)
        (2000, 2)
        >>> thetas[0]  # Vérifions les conditions initiales
        array([ 0.,  6.])
        >>> thetas[-1]  # Dernière valeur ?  # doctest: +ELLIPSIS
        array([ 0.988...,  4.510...])
        >>> maxTheta = max(thetas[:, 0])  # Plus grand angle atteint
        >>> maxTheta  # doctest: +ELLIPSIS
        1.277...
        >>> maxThetaPoint = max(thetas[:, 1])  # Plus grande vitesse atteinte
        >>> maxThetaPoint  # doctest: +ELLIPSIS
        7.013...
        
        - Courbe :math:`(t, \theta(t))`, on constate déjà que ce n'est pas exactement une solution harmonique (il y a une légère amplification due aux erreurs numériques introduites par l'approximation de Taylor qui justifie le schéma de mise à jour de la méthode d'Euler) :
        
        .. image:: TP5__euler_ordre2_position.png
           :align: center
           :scale: 80%
        
        - Portrait de phase (:math:`(\theta(t), \dot\theta(t))`), qui n'est pas un cercle et donc on constate que la méthode d'Euler n'est pas exacte pour cette simulation :
        
        .. image:: TP5__euler_ordre2_vitesse.png
           :align: center
           :scale: 80%
        
        - Complexité en *temps* : :math:`O(n)`, avec :math:`n = \lceil t_{\max} / h \rceil`.
        
        - Complexité en *mémoire* (question I.2.c) : des constantes, plus un tableau numpy de flottants sur 64 bits (``numpy.float64``, soit 4 octets), de taille exactement :math:`(n, 2)`, avec :math:`n = \lceil t_{\max} / h \rceil` (la taille d'un tableau numpy peut être obtenue avec la fonction :func:`numpy.shape`).
        - Exemple :
        
        >>> h = 0.001; tmax = 60
        >>> n = int(np.ceil(tmax / h))
        >>> memoire = 4 * 2 * n  # Pour le numpy array de taille (n, 2)
        >>> print("La procedure euler consommera de l'ordre de %g octets..." % memoire)
        La procedure euler consommera de l'ordre de 480000 octets...
        >>> print("Soit environ %g Ko (ca reste tres raisonnable !)." % (memoire / 1024))
        Soit environ 468.75 Ko (ca reste tres raisonnable !).
    
    f(Y, l=0.3, g=9.80665)
        L'opérateur :math:`f: Y \mapsto f(Y)` :eq:`operateur_f`, écrit comme une fonction Python.
        
        .. math::
           :label: operateur_f
        
           \frac{dY}{dt} &= \Big[\frac{d\theta}{dt}, \frac{d^2\theta}{dt^2}\Big] \\
                         &= [\dot\theta, - (g/l) \sin(\theta)] \\
                    f(Y) &= [Y_1, - (g/l) \sin(Y_0)]
        
        
        - *Arguments :*
           - ``Y`` est tableau ``numpy`` monodimensionnel (à deux éléments) et renvoie un tableau numpy de même taille. :math:`Y = [\theta, \dot\theta]` donc :math:`Y_0 = \theta, Y_1 = \dot\theta`,
           - (optionnel) ``g`` est l'accélération de la pesanteur à la surface de la Terre (:math:`g = 9.80665 \; m/s^2`),
           - (optionnel) ``l`` est la longueur de la tige (:math:`l = 30` cm dans les applications).
        
        - Exemple :
        
        >>> Y = np.array([0, 10]); print(Y)
        [ 0 10]
        >>> l = 0.30; g = 9.80665
        >>> print(f(Y, l, g))
        [10  0]
        >>> Y = np.array([10, 5])
        >>> print(f(Y, l, g))
        [ 5 17]
        >>> l = 0.60  # Une corde plus longue => plus faible vitesse
        >>> print(f(Y, l, g))
        [5 8]
    
    periode1(h, thetas)
        Calcule une valeur approchée de la période :math:`T` du système dynamique supposé en régime sous-critique.
        
        À cette fin, on observera que :math:`\theta(T /2) = 0` et :math:`\theta(t) > 0` pour :math:`t \in ]0, T/2[`.
        Il suffira alors de déterminer de façon *approchée* le plus petit :math:`t > 0` tel que :math:`\theta(t) = 0` : ::
        
            angles = thetas[1:, 0]  # On ignore theta0 = 0
            indice_min_theta = np.argmin(np.abs(angles))
            T = 2 * imin * h
        
        
        - Exemple :
        
        >>> g = 9.80665; l = 0.30  # 30 cm
        >>> w0 = pulsation_propre(l, g)
        >>> k_critique = 2 * w0
        >>> theta0 = 0; thetaPoint0 = 6
        >>> assert thetaPoint0 < k_critique, "Erreur le regime n'est pas sous-critique !"
        >>> # C'est bon, on est en regime sous-critique.
        >>> tmax = 5
        >>> nbPoints = 1000  # On choisit 2000 points
        >>> h = tmax / nbPoints
        >>> thetas = pointMilieu(h, tmax, theta0, thetaPoint0)
        >>> T1 = periode1(h, thetas)
        >>> T1  # doctest: +ELLIPSIS
        1.1799...
        >>> T_ideal = (2 * np.pi) / w0
        >>> T_ideal  # doctest: +ELLIPSIS
        1.0989...
    
    periode2(theta0, thetaPoint0, l=0.3, g=9.80665)
        Calcule une valeur approchée de la période :math:`T` du système dynamique supposé en régime sous-critique.
        
        On calcule :math:`\omega_0`, puis :math:`\cos(\theta_{\max})` par :eq:`cos_theta_max` qu'on inverse pour avoir :math:`\theta_{\max}` l'amplitude maximale des oscillations :
        
        .. math:: \cos(\theta_{max}) = \cos(\theta_0) - \frac{\dot{\theta_0}^2}{2 \omega_0^2}.
           :label: cos_theta_max
        
        
        Puis calcule :math:`T` via cette formule :eq:`integraleT` utilisant une intégrale généralisée :
        
        .. math::
           :label: integraleT
        
           T = \sqrt{\frac{8 l}{g}} \left(\int_{0}^{\theta_{\max}} \frac{1}{\sqrt{\cos(\theta) - \cos(\theta_{\max})}} \mathrm{d}\theta\right).
        
        .. note::
        
           On utilisera la fonction :func:`scipy.integrate.quad` (`voir sa documentation ? <https://docs.scipy.org/doc/scipy/reference/generated/scipy.integrate.quad.html>`_), du `même module <https://docs.scipy.org/doc/scipy/reference/integrate.html>`_ :mod:`scipy.integrate`, dont `voici quelques explications <https://docs.scipy.org/doc/scipy/reference/tutorial/integrate.html#integration-scipy-integrate>`_).
           Cette fonction :func:`quad` est une façon très générique de calculer une intégrale numériquement.
           Pour des fonctions réelles ou multi-variées, continues ou non, :func:`quad` fonctionne très bien (pour des intégrales définies, mais pas uniquement des domaines bornés).
        
           Voir `cet article Wikipédia sur les méthodes de quadrature de Gauss <https://fr.wikipedia.org/wiki/M%C3%A9thodes_de_quadrature_de_Gauss>`_ pour plus de détails si cela vous intéresse,
           ou `cette page en anglais <http://mec-cs101-integrals.readthedocs.io/en/latest/integrals.html#integrals.gaussian_quad>`_, issue de la correction d'un projet que j'avais donné à mes `élèves indiens <http://www.mahindraecolecentrale.edu.in/>`_ l'an dernier (avril 2015).
        
           On peut même intégrer, par ces méthodes, sur des domaines plus compliqués que des simples hyper-rectangles, voir `ces exemples là <http://mec-cs101-integrals.readthedocs.io/en/latest/integrals.html#integrals.nd_quad>`_ (`l'idée est toute bête mais très astucieuse ! <http://scikit-monaco.readthedocs.io/en/latest/tutorials/getting_started.html#complex-integration-volumes>`_).
        
        
        - Exemple :
        
        >>> theta0 = 0; thetaPoint0 = 6
        >>> T2 = periode2(theta0, thetaPoint0)
        >>> T2  # doctest: +ELLIPSIS
        1.1891...
        >>> g = 9.80665; l = 0.30  # 30 cm
        >>> w0 = pulsation_propre(l, g)
        >>> T_ideal = (2 * np.pi) / w0
        >>> T_ideal  # doctest: +ELLIPSIS
        1.0989...
    
    pointMilieu(h, tmax, theta0, thetaPoint0)
        Résoud numériquement l'équation différentielle d'ordre 2 du pendule simple :eq:`pendule2` par `la méthode du point milieu <http://uel.unisciel.fr/mathematiques/eq_diff/eq_diff_ch04/co/apprendre_ch4_03.html>`_ pour :math:`t \in [0, t_{\max}]` avec un pas de temps :math:`h`.
        
        .. math:: \frac{d^2 \theta}{d t^2}(t) + \frac{g}{l} \sin(\theta(t)) = 0.
           :label: pendule2
        
        
        Pour utiliser cet autre schéma, il nous faut aussi écrire cette équation différentielle (ordinaire d'ordre 2, mais *non linéaire !*) sous la forme :math:`Y'(t) = f(t, Y)`, où :math:`Y = [\theta, \dot\theta]` :
        
        .. math:: f(Y) = [Y_1, - (g/l) \sin(Y_0)]
        
        C'est le même opérateur :math:`f` (calculé par la fonction :func:`f` ci-dessus).
        Et dès lors, le *schéma de mise à jour* de *la méthode du point milieu* s'écrit :
        
        .. math::
        
           \begin{cases}
           \Delta_i &= \frac{(t_{i+1} - t_i)}{2} f(t_i, Y_i) \\
           Y_{i+1}  &= Y_i + (t_{i+1} - t_i) \times f(t_i, Y_i + \Delta_i) \\
           \end{cases}
        
        Mais dans notre cas, les temps discrets :math:`t_i` sont uniformément espacés,
        donc :math:`\forall i \in \{0,\dots,n-1\},\; t_{i+1} - t_{i} = h` (le *pas de temps*), donc le schéma se simplifie en :eq:`schemamilieu` :
        
        .. math::
           :label: schemamilieu
        
           \begin{cases}
           \Delta_i &= \frac{h}{2} f(t_i, Y_i) \\
           Y_{i+1}  &= Y_i + h \times f(t_i, Y_i + \Delta_i) \\
           \end{cases}
        
        
        - L'opérateur :math:`f: (t, Y) \mapsto f(t, Y)` est vectoriel (de taille :math:`2 \times 2`), et c'est le même que la méthode d'Euler (donné par la fonction :func:`f`).
        - Les conditions initiales sont données par ``theta0`` et ``thetaPoint0``.
        
        - Renvoit un tableau (``list``) contenant les valeurs de :math:`u_c` calculées aux instants :math:`t_i = i \times h` (pour :math:`0 \leq i \leq t_{\max} / h)`.
        
        - *Arguments* :
           - ``h`` est un pas de temps (:math:`h > 0`),
           - ``tmax`` est la durée totale de la simulation numérique (:math:`t_{\max} > 0`),
           - ``theta0``:math:`= \theta_0` est la valeur de :math:`\theta(t=0)`,
           - et ``thetaPoint0``:math:`= \dot{\theta_0}` est la valeur de :math:`\dot\theta(t=0) = \left(\frac{d\theta}{dt}\right)(t=0)`.
        
        - *Hypothèse* : On résoudra l'équation pour :math:`t \geq 0`.
        - *Résultat* : Elle est d'ordre 2, donc plus précise que la méthode d'Euler qui est seulement d'ordre 1.
        
        - Exemple (question I.3.a) :
        
        >>> theta0 = 0
        >>> thetaPoint0 = 6
        >>> tmax = 5
        >>> nbPoints = 2000  # On choisit 2000 points
        >>> h = tmax / nbPoints
        >>> thetas = pointMilieu(h, tmax, theta0, thetaPoint0)
        >>> thetas[0]  # Vérifions les conditions initiales
        array([ 0.,  6.])
        >>> thetas[-1]  # Dernière valeur ?  # doctest: +ELLIPSIS
        array([ 1.058...,  1.631...])
        >>> maxTheta = max(thetas[:, 0])  # Plus grand angle atteint
        >>> maxTheta  # doctest: +ELLIPSIS
        1.104...
        >>> maxThetaPoint = max(thetas[:, 1])  # Plus grande vitesse atteinte
        >>> maxThetaPoint  # doctest: +ELLIPSIS
        6.0...
        
        - Courbe :math:`(t, \theta(t))`, cette fois ça ressemble beaucoup plus à une solution harmonique idéale (ni amortie ni amplifiée) :
        
        .. image:: TP5__ptmilieu_position.png
           :align: center
           :scale: 80%
        
        - Portrait de phase (:math:`(\theta(t), \dot\theta(t))`), sur lequel on constate avec plaisir que la méthode du point milieu est, elle, parfaitement exacte pour la résolution de cette équation différentielle :eq:`pendule2` :
        
        .. image:: TP5__ptmilieu_vitesse.png
           :align: center
           :scale: 80%
        
        - Complexités : :math:`O(n)` en temps, :math:`O(n)` en mémoire, avec :math:`n = \lceil t_{\max} / h \rceil`.
        
        
        .. seealso:: D'autres références sur la méthode du point milieu :
        
           - `Ces slides <http://wwwens.aero.jussieu.fr/lefrere/master/mni/mncs/cours/equa-diff.pdf#page=13>`_ (slide 13),
           - `Ce sujet de TP <http://www.math.u-bordeaux1.fr/~cdossal/Enseignements/L3-AnaNum/TPEDO1.pdf>`_ (§ 3),
           - `Ces autres slides <http://maverick.inria.fr/~Nicolas.Holzschuch/cours/class5.pdf>`_,
           - (et rien sur Wikipédia, mais... n'hésitez pas à participer à Wikipédia et créer ou modifier une page sur la méthode du point milieu).
    
    portraitPhase(thetas, methode="par m\xc3\xa9thode d'Euler")
        Trace le portrait de phase, soit la dérivée :math:`\dot\theta` en fonction de la valeur de l'angle :math:`\theta`.
        
        - *Argument*:
           - ``theta`` est un *array-like* (liste ou numpy array) de taille :math:`(n, 2)`, qui contient les valeurs (approchées ou non) de :math:`Y = [\theta, \dot\theta]` aux instants :math:`t_i`.
        
        .. image:: TP5__euler_ordre2_vitesse.png
           :align: center
           :scale: 120%
    
    portraitsPhase(kmin=1, kmax=15, tmax=2)
        Utilise :func:`scipy.integrate.odeint` pour afficher un portrait de phase du pendule simple comportant plusieurs trajectoires.
        
        - On prendra pour cela :math:`t_{\max} = 2 s`, :math:`\theta_0 = 0`, et :math:`\dot{\theta_0} = k \; \text{rad} / s`, pour :math:`k \in [1,.., 15]`.
        
        .. note:: Remarque sur les trois régimes (sous-critique, sur-critique, critique).
        
           - Lorsque :math:`\theta_0 = 0` est assez petit (:math:`\dot{\theta_0} < \dot{\theta}_{\text{critique}}`), le pendule effectue des oscillations autour de la position d'équilibre stable :math:`\theta_{\infty} = 0` (**régime sous-critique**),
           - Lorsque :math:`\theta_0 = 0` est assez grand (:math:`\dot{\theta_0} > \dot{\theta}_{\text{critique}}`), il a un mouvement de révolution autour de son point d'attache (**régime sur-critique**),
           - Dans le **régime critique** (qui se produit lorsque :math:`\theta_0 = 0 = 2\omega_0` ), le pendule converge infiniment lentement vers la position d'équilibre instable (:math:`\theta = \pi`). Ce dernier cas est théorique, *il ne s'observe jamais en pratique*.
        
        
        - Dans nos exemples, :math:`2\omega_0 \simeq 11.4`, donc si on choisit des valeurs entières entre 8 et 15 (inclus), 4 seront en régime sous-critique (8,9,10,11), soit 4 cercles ou ellipses, et 4 seront en régime sur-critique (12,13,14,15), soit 4 trajectoires sinusoïdales dans le portrait de phase.
        
        >>> g = 9.80665; l = 0.30  # 30 cm
        >>> w0 = pulsation_propre(l, g)
        >>> k_critique = 2 * w0
        >>> k_critique  # doctest: +ELLIPSIS
        11.434...
        
        - En plus pour ce graphique, j'ai ajouté le régime critique pour bien le visualiser (en vert sombre). On constate effectivement que le pendule converge vers l'équilibre (instable) où :math:`\theta = \pi` :
        
        .. image:: TP5__15_portraits_de_phase.png
           :align: center
           :scale: 90%
    
    pulsation_propre(l=0.3, g=9.80665)
        :math:`\omega_0 = \sqrt{g / l}` est la **pulsation propre du système harmonique** (voir `cette page pour plus de détails <https://fr.wikipedia.org/wiki/Oscillateur_harmonique>`_).
        
        - Exemple :
        
        >>> g = 9.80665; l = 0.30  # 30 cm
        >>> w0 = pulsation_propre(l, g)
        >>> w0  # doctest: +ELLIPSIS
        5.717...
    
    tracerCourbe(listeTemps, theta, methode="par m\xc3\xa9thode d'Euler")
        Trace la courbe :math:`\theta(t)`, valeur de l'angle :math:`\theta` en fonction du temps :math:`t`.
        
        - *Arguments*:
           - ``listeTemps`` est un *array-like* (liste ou numpy array) contenant les valeurs :math:`t_i`,
           - ``theta`` contient les valeurs (approchées ou non) de :math:`\theta_i = \theta(t_i)`.
        
        .. image:: TP5__euler_ordre2_position.png
           :align: center
           :scale: 120%

DATA
    division = _Feature((2, 2, 0, 'alpha', 2), (3, 0, 0, 'alpha', 0), 8192...
    g = 9.80665
    h = 0.0025
    l = 0.3
    methode = u"par methode d'Euler"
    nbPoints = 2000
    print_function = _Feature((2, 6, 0, 'alpha', 2), (3, 0, 0, 'alpha', 0)...
    theta0 = 0
    thetaPoint0 = 6
    tmax = 5


