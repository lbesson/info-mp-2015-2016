`TP8 : dernier TP avant les écrits <../TP8.pdf>`_
-------------------------------------------------

Révisions de tout le programme, séance de questions, révisions de la syntaxe et des concepts clés.
Basé sur le sujet Mines-Ponts MP/PC/PSI 2015.

Le sujet étudiait un système de tests automatisés pour une usine d'imprimantes.

--------------------------------------------------------------------------------

Documentation
^^^^^^^^^^^^^

.. automodule:: TP8
   :members:
   :undoc-members:

--------------------------------------------------------------------------------

Sortie du script
^^^^^^^^^^^^^^^^

.. runblock:: console

   $ python TP8.py

Le fichier Python se trouve ici : :download:`TP8.py`.
