************* Module TP3
W: 33,32: Redefining name 'i' from outer scope (line 215) (redefined-outer-name)
W: 52,16: Redefining name 'j' from outer scope (line 215) (redefined-outer-name)
W: 31, 4: Redefining name 'n' from outer scope (line 141) (redefined-outer-name)
W: 35, 4: Redefining name 'p' from outer scope (line 142) (redefined-outer-name)
W: 25,23: Redefining name 'laby' from outer scope (line 15) (redefined-outer-name)
W:101,32: Redefining name 'i' from outer scope (line 215) (redefined-outer-name)
W: 96,19: Redefining name 'p' from outer scope (line 142) (redefined-outer-name)
W:104,12: Redefining name 'j' from outer scope (line 215) (redefined-outer-name)
W: 96,16: Redefining name 'n' from outer scope (line 141) (redefined-outer-name)
W:129,25: Redefining name 'n' from outer scope (line 141) (redefined-outer-name)
W:129,28: Redefining name 'p' from outer scope (line 142) (redefined-outer-name)
W:132, 8: Redefining name 'k' from outer scope (line 214) (redefined-outer-name)
W:132, 8: Unused variable 'k' (unused-variable)
W:175, 8: Redefining name 'i' from outer scope (line 215) (redefined-outer-name)
W:176,12: Redefining name 'j' from outer scope (line 215) (redefined-outer-name)
W:167, 4: Redefining name 'n' from outer scope (line 141) (redefined-outer-name)
W:253,25: Redefining name 'k' from outer scope (line 214) (redefined-outer-name)
W:253,22: Redefining name 'j' from outer scope (line 215) (redefined-outer-name)
W:257, 4: Redefining name 'n' from outer scope (line 141) (redefined-outer-name)
W:267, 8: Redefining name 'i' from outer scope (line 215) (redefined-outer-name)
W:302, 8: Redefining name 'i' from outer scope (line 215) (redefined-outer-name)
W:288,23: Redefining name 'k' from outer scope (line 214) (redefined-outer-name)
W:288,20: Redefining name 'j' from outer scope (line 215) (redefined-outer-name)
W:292, 4: Redefining name 'n' from outer scope (line 141) (redefined-outer-name)


Report
======
164 statements analysed.

Statistics by type
------------------

+---------+-------+-----------+-----------+------------+---------+
|type     |number |old number |difference |%documented |%badname |
+=========+=======+===========+===========+============+=========+
|module   |1      |1          |=          |100.00      |0.00     |
+---------+-------+-----------+-----------+------------+---------+
|class    |0      |0          |=          |0           |0        |
+---------+-------+-----------+-----------+------------+---------+
|method   |0      |0          |=          |0           |0        |
+---------+-------+-----------+-----------+------------+---------+
|function |7      |7          |=          |100.00      |100.00   |
+---------+-------+-----------+-----------+------------+---------+



External dependencies
---------------------
::

    matplotlib 
      \-pyplot (TP3)
    numpy (TP3)



Raw metrics
-----------

+----------+-------+------+---------+-----------+
|type      |number |%     |previous |difference |
+==========+=======+======+=========+===========+
|code      |181    |58.58 |181      |=          |
+----------+-------+------+---------+-----------+
|docstring |25     |8.09  |25       |=          |
+----------+-------+------+---------+-----------+
|comment   |35     |11.33 |35       |=          |
+----------+-------+------+---------+-----------+
|empty     |68     |22.01 |68       |=          |
+----------+-------+------+---------+-----------+



Duplication
-----------

+-------------------------+------+---------+-----------+
|                         |now   |previous |difference |
+=========================+======+=========+===========+
|nb duplicated lines      |0     |0        |=          |
+-------------------------+------+---------+-----------+
|percent duplicated lines |0.000 |0.000    |=          |
+-------------------------+------+---------+-----------+



Messages by category
--------------------

+-----------+-------+---------+-----------+
|type       |number |previous |difference |
+===========+=======+=========+===========+
|convention |0      |0        |=          |
+-----------+-------+---------+-----------+
|refactor   |0      |0        |=          |
+-----------+-------+---------+-----------+
|warning    |24     |24       |=          |
+-----------+-------+---------+-----------+
|error      |0      |0        |=          |
+-----------+-------+---------+-----------+



Messages
--------

+---------------------+------------+
|message id           |occurrences |
+=====================+============+
|redefined-outer-name |23          |
+---------------------+------------+
|unused-variable      |1           |
+---------------------+------------+



Global evaluation
-----------------
Your code has been rated at 8.54/10 (previous run: 8.54/10, +0.00)
That's pretty good. Good work mate.

