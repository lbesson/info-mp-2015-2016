`TP4 : Récursivité <../TP4.pdf>`_
---------------------------------
Ce TP s'étendra sur deux séances.

.. image:: TP4_courbe_du_dragon_n=15.png
.. image:: solution_1_pb_8_reines.png

Documentation
^^^^^^^^^^^^^
.. automodule:: TP4
   :members:
   :undoc-members:

--------------------------------------------------------------------------------

Sortie du script
^^^^^^^^^^^^^^^^
.. runblock:: console

   $ python TP4.py

Le fichier Python se trouve ici : :download:`TP4.py`.

--------------------------------------------------------------------------------

Courbes du dragon
^^^^^^^^^^^^^^^^^
.. image:: TP4_courbe_du_dragon_n=3.png
.. image:: TP4_courbe_du_dragon_n=6.png
.. image:: TP4_courbe_du_dragon_n=8.png
.. image:: TP4_courbe_du_dragon_n=10.png
.. image:: TP4_courbe_du_dragon_n=12.png
.. image:: TP4_courbe_du_dragon_n=13.png
.. image:: TP4_courbe_du_dragon_n=15.png
