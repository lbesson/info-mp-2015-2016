`TP3 : Parcours de graphes et percolation <../TP3.pdf>`_
--------------------------------------------------------
Milieux plus ou moins poreux
^^^^^^^^^^^^^^^^^^^^^^^^^^^^
.. image:: TP3__Exemple_milieu_peu_poreux.png
.. image:: TP3__Exemple_milieu_poreux.png
.. image:: TP3__Exemple_milieu_très_poreux.png

--------------------------------------------------------------------------------

Documentation
^^^^^^^^^^^^^
.. automodule:: TP3
   :members:
   :undoc-members:
   :show-inheritance:

--------------------------------------------------------------------------------

Sortie du script
^^^^^^^^^^^^^^^^
.. runblock:: console

   $ python TP3.py

Le fichier Python se trouve ici : :download:`TP3.py`.


.. note:: L'étude du phénomène de percolation est encore très active, en mathématiques comme en physique (e.g. `cet article datant du 30 mai 2016 <https://arxiv.org/pdf/1605.08605v1.pdf>`_).
