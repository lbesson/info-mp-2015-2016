TP Informatique pour tous - Prépa MP Lakanal
============================================
Ci dessous se trouvent les corrections des différents Travaux Pratiques
(des liens rapides se trouvent aussi dans la *barre latérale*).

Ces Travaux Pratiques concernent l'enseignement d'informatique *« pour tous »* donné en classe de MP au Lycée Lakanal (en 2015-16).

-----------------------------------------------------------------------------

.. note:: Ces corrections demandent les modules `numpy <http://www.numpy.org/>`_ et `matplotlib <http://www.matplotlib.org/>`_.

   - Gratuitement, vous pouvez télécharger et installer la distribution `Pyzo <http://www.pyzo.org/downloads.html>`_ pour tester ces solutions sur votre ordinateur personnel.
   - Les programmes sont en `Python 3 <https://docs.python.org/3/>`_, mais devraient être valide pour `Python 2 <https://docs.python.org/2/>`_ si besoin.
   - (Plus de détails sur l'`installation de Python <http://perso.crans.org/besson/apprendre-python.fr.html>`_ si besoin.)

.. warning:: Ces solutions sont en accès libre, mais pas  `les sujets <http://psi.lakanal.perso.sfr.fr/eleves/info/TP/>`_..

.. seealso:: `Solutions aux TD <../../TDs/solutions/>`_ ou `solutions aux DS <../../DSs/solutions/>`_.

-----------------------------------------------------------------------------

`TP1 : Remise en forme <../TP1.pdf>`_
--------------------------------------
- *Dates :* jeudi 17-09 et vendredi 18-10 (2015).
- Voir la page correspondante : `<TP1.html>`_.

-----------------------------------------------------------------------------

`TP2 : Algorithmes et complexité <../TP2.pdf>`_
------------------------------------------------
- *Dates :* mercredi 30-09 et jeudi 01-10 (2015).
- Voir la page correspondante : `<TP2.html>`_.

-----------------------------------------------------------------------------

`TP3 : Parcours de graphes et percolation <../TP3.pdf>`_
---------------------------------------------------------
- *Dates :* jeudi 15-10 et 05-11 (2015).
- Voir la page correspondante : `<TP3.html>`_.

-----------------------------------------------------------------------------

`TP4 : Récursivité <../TP4.pdf>`_
----------------------------------
- *Dates :* mercredi 18-11 et jeudi 19-11, puis jeudi 03-11 et 17-11 (2015).
- Voir la page correspondante : `<TP4.html>`_.
- *Remarque :* ce TP occupera deux séances, jusqu'aux vacances de Noël.

.. seealso::

   `Un sapin de Noël clignotant, tracé en Python ? <sapinNoel.html>`_

-----------------------------------------------------------------------------

`TP5 : résolution numérique d'équations différentielles <../TP5.pdf>`_
----------------------------------------------------------------------
- *Dates :* mercredi 13 et jeudi 14 janvier (*2016*).
- Voir la page correspondante : `<TP5.html>`_.

-----------------------------------------------------------------------------

Le `TP6 <../../DSs/solutions/Centrale_MP__2015.html>`_ était un sujet de concours, travaillé à l'écrit.

-----------------------------------------------------------------------------

`TP7 : algorithmes de tris <../TP7.pdf>`_
-----------------------------------------
- *Dates :* 18 février et 10 mars (2016).
- Voir la page correspondante : `<TP7.html>`_.

-----------------------------------------------------------------------------

.. attention:: Ne trichez pas en lisant les solutions à l'avance, ce n'est pas le but.


`TD/TP 8 : dernier TP avant les écrits <../TP8.pdf>`_ !
-------------------------------------------------------
- Sujet : révisions de tout le programme, séance de questions, révisions de la syntaxe et des concepts clés.
- On étudiera en détail un sujet de concours récent, adapté pour tenir sur deux séances et faire réviser tout le programme.
- *Dates 1/2 :* jeudi 17 et 24 mars (2016).
- *Dates 2/2 :* mercredi 30 et jeudi 31 mars (2016).
- À venir...

-----------------------------------------------------------------------------

Table des matières
==================
* :ref:`genindex`,
* :ref:`modindex`,
* Nouveau : :ref:`classindex`,
* Nouveau : :ref:`funcindex`,
* Nouveau : :ref:`dataindex`,
* Nouveau : :ref:`excindex`,
* Nouveau : :ref:`methindex`,
* Nouveau : :ref:`classmethindex`,
* Nouveau : :ref:`staticmethindex`,
* Nouveau : :ref:`attrindex`,
* :ref:`search`.

.. toctree::
   :maxdepth: 4

   TP1
   TP2
   TP3
   TP4
   sapinNoel
   TP5
   TP7
   TP8

-----------------------------------------------------------------------------

Remarques
=========
- J'essaie de suivre les conventions de style et de syntaxe que les examinateurs des concours suivront et attendront lors des écrits et des oraux de concours.
- J'essaie de ne pas utiliser de notions, d'éléments de la syntaxe Python ni de modules hors-programme.
- Une exception à cette règle est le mot clé Python `assert <https://docs.python.org/3/reference/simple_stmts.html#assert>`_, pas très dur à comprendre mais bien pratique (il permet simplement de *vérifier une assertion*, par exemple ``assert n >= 0`` s'assure que l'entier ``n`` soit positif avant de continuer, et la fonction ou le programme échouera sinon).


.. hint:: `Me contacter <http://perso.crans.org/besson/callme.fr.html>`_ si besoin ?

   S'il vous plait, n'hésitez pas à me `contacter <http://perso.crans.org/besson/contact>`_
   si besoin, pour signaler une erreur, poser une question ou demander plus de détails sur une correction.
   Par courriel à ``besson à crans point org``
   (et `plus de moyens de me contacter sont sur cette page <http://perso.crans.org/besson/callme.fr.html>`_).

-----------------------------------------------------------------------------

Auteurs et copyrights
=====================
- Les sujets sont rédigés par Arnaud Basson (professeur en PSI\* et MP au Lycée Lakanal),
- Ces sujets concernent l'année scolaire 2015-16 (septembre 2015 - juin 2016),
- Ces solutions et la documentation est faite par `Lilian Besson <http://perso.crans.org/besson/>`_, et mise en ligne sous les termes de la `licence MIT <http://lbesson.mit-license.org/>`_,
- Ces sujets et ces solutions suivent le programme officiel d'« informatique pour tous » en prépa MP, `version 2013 <http://www.education.gouv.fr/pid25535/bulletin_officiel.html?cid_bo=71586>`_,
- Ces pages sont générées par `Sphinx <http://sphinx-doc.org/>`_, l'outil de documentation pour Python.
