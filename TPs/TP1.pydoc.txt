Pour n = 10, s1(n) = s1(10) = 1320
Pour n = 10, s2(n) = s2(10) = 90.0
Pour n = 10, v(10) = v_10 = 3842
Pour p = 3841, le seuil trouve vaut 10.
Pour p = 3842, le seuil trouve vaut 10.
Pour p = 3843, le seuil trouve vaut 11.
Pour i = 0, le ieme nombre de Catalan est 1.
Pour i = 1, le ieme nombre de Catalan est 1.
Pour i = 2, le ieme nombre de Catalan est 2.
Pour i = 3, le ieme nombre de Catalan est 5.
Pour i = 4, le ieme nombre de Catalan est 14.
Pour i = 5, le ieme nombre de Catalan est 42.
Pour i = 6, le ieme nombre de Catalan est 132.
Pour i = 7, le ieme nombre de Catalan est 429.
Pour i = 8, le ieme nombre de Catalan est 1430.
Pour i = 9, le ieme nombre de Catalan est 4862.
Pour i = 2, t = [1, 0], sousMaximum(t) = 0.
Pour i = 3, t = [2, 1, 0], sousMaximum(t) = 1.
Pour i = 4, t = [0, 2, 1, 3], sousMaximum(t) = 2.
Pour i = 5, t = [1, 3, 2, 0, 4], sousMaximum(t) = 3.
Pour i = 6, t = [0, 5, 1, 4, 2, 3], sousMaximum(t) = 4.
Pour i = 7, t = [6, 1, 0, 3, 2, 5, 4], sousMaximum(t) = 5.
Pour i = 8, t = [3, 6, 4, 5, 0, 1, 7, 2], sousMaximum(t) = 6.
Pour i = 9, t = [8, 3, 6, 1, 2, 0, 4, 5, 7], sousMaximum(t) = 7.
[1, 2, 3, 4]
[2, 1, 4, 3]
[1, 2, 3, 4]
[2, 1, 4, 3]
3
Pour 'roucouler', la plus grand sous-chaine repetee est ou
Pour 'tralala', la plus grand sous-chaine repetee est al
Pour 'l'ouie de l'oie de Louis', la plus grand sous-chaine repetee est e de 
Help on module TP1:

NAME
    TP1 - TP1 au Lycée Lakanal (sujet rédigé par Arnaud Basson).

FILE
    /home/lilian/teach/info-mp-2015-2016/TPs/TP1.py

DESCRIPTION
    - *Date :* jeudi 17-09 et vendredi 18-10 (2015),
    - *Auteur :* Lilian Besson, pour le cours d'informatique pour tous en prépa MP (http://perso.crans.org/besson/infoMP/),
    - *Licence :* MIT Licence (http://lbesson.mit-license.org).

FUNCTIONS
    catalan(n)
        Calcule de façon naïve le nième nombre de Catalan.
        
        La complexité spatiale est en :math:`O(n)`, et temporelle en :math:`O(n^2)`.
    
    double()
    
    echangeVoisins1(t)
        Renvoit un nouveau tableau.
    
    echangeVoisins2(t)
        Calcul en place.
    
    estNombre(ch)
        Teste si la chaîne ch est un nombre entier positif ou non (naïvement).
    
    longueur(texte, i, j)
        On suppose :math:`0 \leq i < j < len(texte)`.
        Cette fonction est au pire en :math:`O(j-i)` (soit :math:`O(n)` dans le pire des cas).
    
    produit()
    
    s1(n)
        :math:`s1(n) = \sum_{1 <= i < j <= n} ij`
    
    s2(n)
        :math:`s2(n) = \sum_{0 <= p + q <= n} p / (q + 1)`
    
    seuil(p)
        Trouve par recherche exhaustive le plus petit entier n tel que :math:`v(n) < p`.
        
        Attention, cette fonction n'est *pas du tout* optimisée.
    
    sousChaineConstante(texte)
        En O(n).
    
    sousChaineRepetee(texte)
        Au pire des cas, en :math:`O(n^3)`.
    
    sousMaximum(t)
        Calcule le deuxième plus grand élément d’un tableau de nombres t dont les éléments sont supposés deux à deux distincts.
        
        On effectue un seul parcours du tableau, donc l'algorithme est en :math:`O(n)` (pire cas, meilleur cas, moyenne, bref toutes les complexités).
    
    v(n)
        Calcul le énième terme de la suite récurrente :math:`v_n` définie par :math:`v_0 = 1, v_1 = 2, et v_{n+2} = v_{n+1} + 3 * v_n`.

DATA
    i = 9
    print_function = _Feature((2, 6, 0, 'alpha', 2), (3, 0, 0, 'alpha', 0)...
    t = [2, 1, 4, 3]
    u = [2, 1, 4, 3]


