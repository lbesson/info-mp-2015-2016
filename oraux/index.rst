Oraux - Informatique pour tous - Prépa MP Lakanal
=================================================
Ci dessous se trouvent quelques corrections pour des sujets d'oraux de mathématiques avec Python,
tels que donnés au `concours Centrale-Supélec en 2015 <http://www.concours-centrale-supelec.fr/CentraleSupelec/MultiY/C2015/#oMat2>`_
(des liens rapides se trouvent aussi dans la *barre latérale*).

Ces corrections concernent l'enseignement d'informatique *« pour tous »* donné en classe de MP au Lycée Lakanal (en 2015-16).

-----------------------------------------------------------------------------

Les oraux *(exercices de maths avec Python)*
--------------------------------------------

Pour ceux que ça concerne, des séances de préparation aux oraux de `"maths avec Python" (maths 2) <http://www.concours-centrale-supelec.fr/CentraleSupelec/MultiY/C2015/#oMat2>`_ du concours Centrale Supélec auront lieu les vendredi 10 et samedi 11 juin (2016).

En attendant, je vous invite à lire `cette page avec attention <http://www.concours-centrale-supelec.fr/CentraleSupelec/MultiY/C2015/#oMat2>`_, et à jeter un œil aux documents mis à disposition :

Fiches de révisions *pour les oraux*
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

1. `Calcul matriciel <http://www.concours-centrale-supelec.fr/CentraleSupelec/MultiY/C2015/Python-matrices.pdf>`_, avec `numpy <https://docs.scipy.org/doc/numpy/>`_ et `numpy.linalg <http://docs.scipy.org/doc/numpy/reference/routines.linalg.html>`_ : on s'est déjà bien entraîné,
2. `Réalisation de tracés <http://www.concours-centrale-supelec.fr/CentraleSupelec/MultiY/C2015/Python-plot.pdf>`_, avec `matplotlib <http://matplotlib.org/users/beginner.html>`_ : vous savez déjà bien faire, mais réviser un peu fera du bien,
3. `Analyse numérique <http://www.concours-centrale-supelec.fr/CentraleSupelec/MultiY/C2015/Python-AN.pdf>`_, avec `numpy`_ et `scipy <http://docs.scipy.org/doc/scipy/reference/tutorial/index.html>`_ : on s'est déjà entraîné. Voir par exemple `scipy.integrate <http://docs.scipy.org/doc/scipy/reference/tutorial/integrate.html>`_ avec les fonctions `scipy.integrate.quad <http://docs.scipy.org/doc/scipy/reference/generated/scipy.integrate.quad.html>`_ (intégrale numérique) et `scipy.integrate.odeint <http://docs.scipy.org/doc/scipy/reference/generated/scipy.integrate.odeint.html>`_ (résolution numérique d'une équadiff),
4. `Polynômes <http://www.concours-centrale-supelec.fr/CentraleSupelec/MultiY/C2015/Python-polynomes.pdf>`_ : **ATTENTION** on n'a jamais travaillé avec ça, essayez de le découvrir de votre côté ! Avec `numpy.polynomials <https://docs.scipy.org/doc/numpy/reference/routines.polynomials.package.html>`_, `ce tutoriel peut aider <https://docs.scipy.org/doc/numpy/reference/routines.polynomials.classes.html>`_,
5. `Probabilités <http://www.concours-centrale-supelec.fr/CentraleSupelec/MultiY/C2015/Python-random.pdf>`_, avec `numpy <https://docs.scipy.org/doc/numpy/>`_ et `random <https://docs.python.org/3/library/random.html>`_ : on en a déjà fait un peu en info, et en colles de maths aussi.

Pour réviser : voir `ce tutoriel Matplotlib (en anglais) <http://www.labri.fr/perso/nrougier/teaching/matplotlib/>`_, `ce tutoriel Numpy (en anglais) <http://www.labri.fr/perso/nrougier/teaching/numpy/numpy.html>`_.
Ainsi que tous les `TP <../TPs/solutions/>`_, `TD <../TDs/solutions/>`_ et `DS <../DSs/solutions/>`_ en Python que nous avons fait ensemble depuis septembre !

-----------------------------------------------------------------------------

.. note:: Ces corrections demandent `le module numpy <http://www.numpy.org/>`_ et `le module matplotlib <http://www.matplotlib.org/>`_.

   - Gratuitement, vous pouvez télécharger et installer la distribution `Pyzo <http://www.pyzo.org/downloads.html>`_ pour tester ces solutions sur votre ordinateur personnel.
   - Les programmes sont en `Python 3 <https://docs.python.org/3/>`_, mais devraient être valide pour `Python 2 <https://docs.python.org/2/>`_ si besoin.
   - (Plus de détails sur l'`installation de Python <http://perso.crans.org/besson/apprendre-python.fr.html>`_ si besoin.)

.. note:: Ces solutions sont en accès libre, et les sujets sont disponibles sur `le site du concours Centrale-Supélec <http://www.concours-centrale-supelec.fr/CentraleSupelec/MultiY/C2015/#oMat2>`_.

.. seealso:: `Solutions aux TD <../../TDs/solutions/>`_, `solutions aux TP <../../TPs/solutions/>`_, ou `solutions aux DS <../../DSs/solutions/>`_.

-----------------------------------------------------------------------------

`PSI_Mat2_2015_24 <http://www.concours-centrale-supelec.fr/CentraleSupelec/MultiY/C2015/PSI-Mat2-2015-24.pdf>`_
---------------------------------------------------------------------------------------------------------------
- *Thème :* tracé de courbes, développements limités, calcul d'équivalent.
- Voir la page correspondante : `<PSI_Mat2_2015_24.html>`_.

-----------------------------------------------------------------------------

`PSI_Mat2_2015_25 <http://www.concours-centrale-supelec.fr/CentraleSupelec/MultiY/C2015/PSI-Mat2-2015-25.pdf>`_
---------------------------------------------------------------------------------------------------------------
- *Thème :* algèbre linéaire, construction de matrices, calculs de valeurs propres, polynômes et diagonalisations.
- Voir la page correspondante : `<PSI_Mat2_2015_25.html>`_.

-----------------------------------------------------------------------------

`PSI_Mat2_2015_26 <http://www.concours-centrale-supelec.fr/CentraleSupelec/MultiY/C2015/PSI-Mat2-2015-26.pdf>`_
---------------------------------------------------------------------------------------------------------------
- *Thème :* construction de matrices, calculs de valeurs propres, simulations probabilistes, tracé de courbes, et diagonalisations.
- Voir la page correspondante : `<PSI_Mat2_2015_26.html>`_.

-----------------------------------------------------------------------------

`PC_Mat2_2015_27 <http://www.concours-centrale-supelec.fr/CentraleSupelec/MultiY/C2015/PC-Mat2-2015-27.pdf>`_
-------------------------------------------------------------------------------------------------------------
- *Thème :* simulations probabilistes, calcul asymptotique.
- Voir la page correspondante : `<PC_Mat2_2015_27.html>`_.

-----------------------------------------------------------------------------

`PC_Mat2_2015_28 <http://www.concours-centrale-supelec.fr/CentraleSupelec/MultiY/C2015/PC-Mat2-2015-28.pdf>`_
-------------------------------------------------------------------------------------------------------------
- *Thème :* simulations probabilistes, développements limités, calcul asymptotique, calculs de sommes.
- Voir la page correspondante : `<PC_Mat2_2015_28.html>`_.

-----------------------------------------------------------------------------

Table des matières
==================
* :ref:`genindex`,
* :ref:`modindex`,
* Nouveau : :ref:`classindex`,
* Nouveau : :ref:`funcindex`,
* Nouveau : :ref:`dataindex`,
* Nouveau : :ref:`excindex`,
* Nouveau : :ref:`methindex`,
* Nouveau : :ref:`classmethindex`,
* Nouveau : :ref:`staticmethindex`,
* Nouveau : :ref:`attrindex`,
* :ref:`search`.

.. toctree::
   :maxdepth: 4

   PSI_Mat2_2015_24
   PSI_Mat2_2015_25
   PSI_Mat2_2015_26
   PC_Mat2_2015_27
   PC_Mat2_2015_28

-----------------------------------------------------------------------------

Remarques
=========
- J'essaie de suivre les conventions de style et de syntaxe que les examinateurs des concours suivront et attendront lors des écrits et des oraux de concours.
- J'essaie de ne pas utiliser de notions, d'éléments de la syntaxe Python ni de modules hors-programme.


.. hint:: `Me contacter <http://perso.crans.org/besson/callme.fr.html>`_ si besoin ?

   S'il vous plait, n'hésitez pas à me `contacter <http://perso.crans.org/besson/contact>`_
   si besoin, pour signaler une erreur, poser une question ou demander plus de détails sur une correction.
   Par courriel à ``besson à crans point org``
   (et `plus de moyens de me contacter sont sur cette page <http://perso.crans.org/besson/callme.fr.html>`_).

-----------------------------------------------------------------------------

Auteurs et copyrights
=====================
- Les sujets sont la propriété du `concours Centrale-Supélec <http://www.concours-centrale-supelec.fr/>`_,
- Ces sujets concernent l'année scolaire 2015-16 (septembre 2015 - juin 2016),
- Ces solutions et la documentation est faite par `Lilian Besson <http://perso.crans.org/besson/>`_, et mise en ligne sous les termes de la `licence MIT <http://lbesson.mit-license.org/>`_,
- Ces sujets et ces solutions suivent le programme officiel d'« informatique pour tous » en prépa MP, `version 2013 <http://www.education.gouv.fr/pid25535/bulletin_officiel.html?cid_bo=71586>`_,
- Ces pages sont générées par `Sphinx <http://sphinx-doc.org/>`_, l'outil de documentation pour Python.
