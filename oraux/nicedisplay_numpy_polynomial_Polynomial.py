#! /usr/bin/env python3
# -*- coding: utf-8; mode: python -*-
""" A tiny modification of the method numpy.polynomial.Polynomial.__str__ to nicely display a polynomials.

Cf. https://docs.scipy.org/doc/numpy/reference/generated/numpy.polynomial.polynomial.Polynomial.html for more details about the original class.

- *Online:* https://bitbucket.org/snippets/lbesson/j6dpz#file-nicedisplay_numpy_polynomial_Polynomial.py
- *Author:* Lilian Besson, for http://perso.crans.org/besson/infoMP/oraux/solutions/PSI_Mat2_2015_25.html,
- *License:* MIT Licence (http://lbesson.mit-license.org).

"""

from __future__ import print_function, division  # Python 2 compatibility

from numpy.polynomial import Polynomial


class MyPolynomial(Polynomial):
    """ Small extension of the numpy.polynomial.Polynomial class to change its __str__ method.
    """
    def __str__(self):
        """ Improved __str__ method to print nicely the polynomial as we write it in maths."""
        coefs = self.coef
        res = ""
        for i, a in enumerate(coefs):
            if int(a) == a:  # Remove the trailing .0
                a = int(a)
            if i == 0:
                if a > 0:
                    res += "{a} + ".format(a=a)
                elif a < 0:
                    res += "({a}) + ".format(a=a)
            elif i == 1:
                if a == 1:
                    res += "X + "
                elif a > 0:
                    res += "{a} * X + ".format(a=a)
                elif a < 0:
                    res += "({a}) * X + ".format(a=a)
            else:
                if a == 1:
                    res += "X**{i} + ".format(i=i)
                elif a > 0:
                    res += "{a} * X**{i} + ".format(a=a, i=i)
                elif a < 0:
                    res += "({a}) * X**{i} + ".format(a=a, i=i)
        return res[:-3] if res else ""


P = MyPolynomial
X = P([0, 1])  # We define the monome X, to work with it efficiently
# eg. 1 + 2*X + 17*X**3


def test():
    """ Some tests of our improved ``__str__`` method.

    First, let start by defining the monome X:

    >>> P = MyPolynomial
    >>> X = P([0, 1])  # We define the monome X, to work with it efficiently
    >>> print(X)
    X

    And then a few other polynomials:

    >>> Q1 = 1 + 2*X + 17*X**3
    >>> print(Q1)
    1 + 2 * X + 17 * X**3
    >>> Q2 = Q1 - 2*X
    >>> print(Q2)
    1 + 17 * X**3

    We can check that the negative signs work too:

    >>> Q3 = -1 - 2*X - 17*X**3
    >>> print(Q3)
    (-1) + (-2) * X + (-17) * X**3
    >>> print(-Q3)
    1 + 2 * X + 17 * X**3

    And with crazily long polynomials:

    >>> Q4 = (1 + 2*X + 17*X**3) ** 20
    >>> print(Q4)  # doctest: +ELLIPSIS
    1 + 40 * X + 760 * X**2 + 9460 * X**3 + 90440 * X**4 + ... + 4064231406647572451819520 * X**60
    >>> Q5 = (1 + 2*X + 17*X**3) ** 100
    >>> print(Q5)  # doctest: +ELLIPSIS
    1 + 200 * X + 19800 * X**2 + 1295300 * X**3 + ... + 1108899372780782949283060780215564719143115314569706274981249422854766735454757111223455545827324114733078454801811428605952 * X**300

    It works nicely !
    """
    pass


if __name__ == '__main__':
    from doctest import testmod
    print("Automatic test of all the docstrings:")
    testmod(verbose=True)
    print("\nMore details on these doctests can be found online in the official Python documentation:\nhttps://docs.python.org/3/library/doctest.html")
# End of the file
