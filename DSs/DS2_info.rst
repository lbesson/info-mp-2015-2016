`DS 4 : informatique pour tous en MP <../DS2_info.pdf>`_
========================================================

4ème DS, portant sur l'étude de bout en bout d'un problème d'ingénierie numérique réelle (installation d'un réseau connecté de fissuromètre).

.. image:: DS2_figure_1.png
   :width: 90%

Le sujet parlait de :

1. traitement du signal, débruitage de signal,
2. résolution numérique d'une équation différentielle (linéaire, ordre 2, avec second membre),
3. transformée de Fourier (discrète DFT, et rapide FFT),
4. utilisation d'une base de données.

Ces différents blos sont mis ensembles selon ce schéma :

.. image:: DS2_figure_2.png
   :width: 100%


Étude d’un capteur de modification de fissure
---------------------------------------------

.. automodule:: DS2_info
    :members:
    :undoc-members:

--------------------------------------------------------------------------------

Conseils (répétés)
------------------
- Soignez la présentation de vos copies,
- Essayez d'être très attentif à la syntaxe de Python (il y a eu trop d'erreurs d'indentation et de ``:`` manquants),
- Vous devez être plus attentif aux consignes de l'énoncé (certains élèves oublient de donner la complexité, ou répondent seulement à la moitié de la question),
- Comme dans chaque concours/DS, vous devez essayer de *"grapiller"* des points là où vous peuvez.

.. note:: Ce sujet durait 2h, et il était possible et raisonnable (presque) tout faire !

--------------------------------------------------------------------------------

Sortie du script
----------------
.. runblock:: console

   $ python3 DS2_info.py

Le fichier Python se trouve ici : :download:`DS2_info.py`.
