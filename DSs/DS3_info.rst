`DS 5 : X PSI-PT 2010 <../DS3_info.pdf>`_
=========================================
5ème DS, **rechercher un mot dans un texte et compter ses occurrences**.

Le sujet est une ré-écriture du sujet X PSI-PT de 2010. L'accent a été mis sur la partie II pour la rendre plus intéressante.

Le sujet commençait par une petite introduction, motivant le problème algorithmique de recherche d'un mot dans un texte (recherche d'une sous-chaîne) par la quantité toujours croissante d'informatique disponible en ligne.

.. automodule:: DS3_info
    :members:
    :undoc-members:


--------------------------------------------------------------------------------

Conseils
--------
- Améliorez la présentation de vos copies,
- Essayez d'être encore plus attentif à la syntaxe de Python (il y a eu trop d'erreurs d'indentation et de ``:`` manquants),
- Vous devez être plus attentif aux consignes de l'énoncé (certains élèves oublient de donner la complexité dans les dernières questions),
- Comme dans chaque concours/DS, vous devez essayer de *"grapiller"* des points là où vous peuvez.

--------------------------------------------------------------------------------

Sortie du script
----------------
.. runblock:: console

   $ python3 DS3_info.py

Le fichier Python se trouve ici : :download:`DS3_info.py`.
