`DS 3 : Centrale MP 2015 <../Centrale_MP__2015.pdf>`_
=====================================================
3ème TD/DS, algorithmique sur des listes, puis schémas numériques (Euler, Verlet), appliqué à un problème de mécanique à N corps, et enfin un dernier exercice avec une base de données (et des requêtes SQL).

.. automodule:: Centrale_MP__2015
    :members:
    :undoc-members:


--------------------------------------------------------------------------------

Conseils
--------
- Soignez la présentation de vos copies,
- Essayez d'être très attentif à la syntaxe de Python (il y a eu trop d'erreurs d'indentation et de ``:`` manquants),
- Vous devez être plus attentif aux consignes de l'énoncé (certains élèves oublient de donner la complexité, ou répondent seulement à la moitié de la question),
- Comme dans chaque concours/DS, vous devez essayer de *"grapiller"* des points là où vous peuvez.

.. note:: Ce sujet durait 3h et on n'a eu que 2h en TD/DS, donc c'est normal de ne pas avoir pu tout faire !

--------------------------------------------------------------------------------

Sortie du script
----------------
.. runblock:: console

   $ python3 Centrale_MP__2015.py

Le fichier Python se trouve ici : :download:`Centrale_MP__2015.py`.
