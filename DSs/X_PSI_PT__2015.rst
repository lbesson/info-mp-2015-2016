`DS 2 : X PSI-PT 2015 <../X_PSI_PT__2015.pdf>`_
===============================================
2ème DS, algorithmique sur des tableaux 1D et 2D, et simulations probabilistes.

.. automodule:: X_PSI_PT__2015
    :members:
    :undoc-members:


--------------------------------------------------------------------------------

Conseils
--------
- Améliorez la présentation de vos copies,
- Essayez d'être encore plus attentif à la syntaxe de Python (il y a eu trop d'erreurs d'indentation et de ``:`` manquants),
- Vous devez être plus attentif aux consignes de l'énoncé (certains élèves oublient de donner la complexité dans les dernières questions),
- Comme dans chaque concours/DS, vous devez essayer de *"grapiller"* des points là où vous peuvez (la `Q09 <./X_PSI_PT__2015.html#X_PSI_PT__2015.coloriageAleatoire>`_ (:py:func:`X_PSI_PT__2015.coloriageAleatoire`) et `Q13 <./X_PSI_PT__2015.html#X_PSI_PT__2015.existeCheminSimple>`_ (:py:func:`X_PSI_PT__2015.existeCheminSimple`) étaient faciles, par exemple),
- Enfin, vous devez vous forcer à n'utiliser que les structures de données de l'énoncé (c'est flagrant, certains commencent par utiliser :py:func:`X_PSI_PT__2015.creerTableau` ou :py:func:`X_PSI_PT__2015.creerListeVide` au début, puis finissent avec des ``[]`` et :py:meth:`list.append()` à tout va), probablement par manque de concentration et à cause de la fatigue.

--------------------------------------------------------------------------------

Sortie du script
----------------
.. runblock:: console

   $ python3 X_PSI_PT__2015.py

Le fichier Python se trouve ici : :download:`X_PSI_PT__2015.py`.
