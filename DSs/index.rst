DS Informatique pour tous - Prépa MP Lakanal
============================================
Ci dessous se trouvent les corrections des différents Devoirs Surveillés (DS)
(des liens rapides se trouvent aussi dans la *barre latérale*).

Ces Devoirs Surveillés concernent l'enseignement d'informatique *« pour tous »* donné en classe de MP au Lycée Lakanal (en 2015-16).

-----------------------------------------------------------------------------

.. note:: Ces corrections demandent des fois les modules `numpy <http://www.numpy.org/>`_ et `matplotlib <http://www.matplotlib.org/>`_.

   - Gratuitement, vous pouvez télécharger et installer la distribution `Pyzo <http://www.pyzo.org/downloads.html>`_ pour tester ces solutions sur votre ordinateur personnel.
   - Les programmes sont en `Python 3 <https://docs.python.org/3/>`_, mais devraient être valide pour `Python 2 <https://docs.python.org/2/>`_ si besoin.
   - (Plus de détails sur l'`installation de Python <http://perso.crans.org/besson/apprendre-python.fr.html>`_ si besoin.)

.. warning:: Ces solutions sont en accès libre, mais pas tous `les sujets <http://psi.lakanal.perso.sfr.fr/eleves/info/devoirs/>`_.

.. seealso:: `Solutions aux TD <../../TDs/solutions/>`_ ou `solutions aux TP <../../TPs/solutions/>`_.

-----------------------------------------------------------------------------

`DS 1 <http://psi.lakanal.perso.sfr.fr/eleves/info/devoirs/interroOct_MP.pdf>`_
-------------------------------------------------------------------------------
- *Date :* octobre (2015).
- `Correction DS 1 <http://psi.lakanal.perso.sfr.fr/eleves/info/devoirs/interro_MP_corr.pdf>`_.
- *Pas encore de correction ici*.

.. note:: Rappels :

   - Durant un devoir écrit d'informatique, les calculatrices sont interdites.
   - `Python <https://www.python.org/>`_ est le langage obligatoire pour les écrits et les oraux des concours scientifiques désormais (aux Mines, Centrale et X/ENS, et Scilab est aussi autorisé pour les CCP).

-----------------------------------------------------------------------------

`DS 2 : X PSI-PT 2015 <../X_PSI_PT__2015.pdf>`_
-----------------------------------------------
- *Date :* vendredi 11-12 (2015).
- Voir la page correspondante : `<X_PSI_PT__2015.html>`_ (pour tous les détails et des exemples).
- Et ce fichier pour juste le code : `<_modules/X_PSI_PT__2015__minimal_solution.html>`_.

.. image:: X_PSI_PT__2015__plan3.png
   :target: X_PSI_PT__2015.html#existeCheminArcEnCiel

-----------------------------------------------------------------------------

`TD/DS 3 : Centrale MP 2015 <../Centrale_MP__2015.pdf>`_
--------------------------------------------------------
- *Date :* mercredi 27 et jeudi 28 janvier (2016), fait en classe tous ensemble.
- Voir la page correspondante : `<Centrale_MP__2015.html>`_ (pour tous les détails, les preuves, et des exemples).
- Et ce fichier pour juste le code : `<_modules/Centrale_MP__2015.html>`_ (très détaillé).

-----------------------------------------------------------------------------

`DS 4 : Étude d'un capteur de modification de fissure <../DS2_info.pdf>`_
-------------------------------------------------------------------------
- *Date :* vendredi 05 février (2016).
- Voir la page correspondante : `<DS2_info.html>`_ (pour tous les détails, les preuves, et des exemples).
- Et ce fichier pour juste le code : `<_modules/DS2_info.html>`_ (très détaillé).

-----------------------------------------------------------------------------

`DS 4 : X PSI-PT 2010 <../DS3_info.pdf>`_
-----------------------------------------
- *Date :* mardi 22 mars (2016).
- Voir la page correspondante : `<DS3_info.html>`_ (pour tous les détails, les preuves, et des exemples).
- Et ce fichier pour juste le code : `<_modules/DS3_info.html>`_ (très détaillé).

-----------------------------------------------------------------------------

Table des matières
==================
* :ref:`genindex`,
* :ref:`modindex`,
* Nouveau : :ref:`classindex`,
* Nouveau : :ref:`funcindex`,
* Nouveau : :ref:`dataindex`,
* Nouveau : :ref:`excindex`,
* Nouveau : :ref:`methindex`,
* Nouveau : :ref:`classmethindex`,
* Nouveau : :ref:`staticmethindex`,
* Nouveau : :ref:`attrindex`,
* :ref:`search`.

.. toctree::
   :maxdepth: 4

   X_PSI_PT__2015
   X_PSI_PT__2015__minimal_solution
   Afficher_un_histogramme_de_notes_DS2
   Centrale_MP__2015
   DS2_info
   Afficher_un_histogramme_de_notes_DS4
   DS3_info


-----------------------------------------------------------------------------

Remarques
=========
- J'essaie de suivre les conventions de style et de syntaxe que les examinateurs des concours suivront et attendront lors des écrits et des oraux de concours.
- J'essaie de ne pas utiliser de notions, d'éléments de la syntaxe Python ni de modules hors-programme.


.. hint:: `Me contacter <http://perso.crans.org/besson/callme.fr.html>`_ si besoin ?

   S'il vous plait, n'hésitez pas à me `contacter <http://perso.crans.org/besson/contact>`_
   si besoin, pour signaler une erreur, poser une question ou demander plus de détails sur une correction.
   Par courriel à ``besson à crans point org``
   (et `plus de moyens de me contacter sont sur cette page <http://perso.crans.org/besson/callme.fr.html>`_).

-----------------------------------------------------------------------------

Auteurs et copyrights
=====================
- Les sujets sont rédigés par Arnaud Basson (professeur en PSI\* et MP au Lycée Lakanal) ou sont des sujet de concours non modifiés,
- Ces sujets concernent l'année scolaire 2015-16 (septembre 2015 - juin 2016),
- Ces solutions et la documentation est faite par `Lilian Besson <http://perso.crans.org/besson/>`_, et mise en ligne sous les termes de la `licence MIT <http://lbesson.mit-license.org/>`_,
- Ces sujets et ces solutions suivent le programme officiel d'« informatique pour tous » en prépa MP, `version 2013 <http://www.education.gouv.fr/pid25535/bulletin_officiel.html?cid_bo=71586>`_,
- Ces pages sont générées par `Sphinx <http://sphinx-doc.org/>`_, l'outil de documentation pour Python.
