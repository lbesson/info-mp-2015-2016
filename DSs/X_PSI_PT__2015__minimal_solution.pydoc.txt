

Partie 0 : Fonctions supposées données


Partie I : Préliminaires : Listes dans redondance
Note : une liste non-ordonnée et sans redondance est exactement un set de Python, mais nous ne devons pas nous en servir.


Partie II. Création et manipulation de plans
Partie III. Recherche de chemins arc-en-ciel
Partie IV. Recherche de chemin passant par exactement k villes intermédiaires distinctes
 Question 14 :
Le programme a modifier est existeCheminArcEnCiel.
Il faut faire en sorte de garder en mémoire les listes liste successives.

Help on module X_PSI_PT__2015__minimal_solution:

NAME
    X_PSI_PT__2015__minimal_solution - Correction du DS de vendredi 11 décembre 2015.

FILE
    /home/lilian/teach/info-mp-2015-2016/DSs/X_PSI_PT__2015__minimal_solution.py

DESCRIPTION
    Voici ci-dessous la documentation de la correction complète du sujet d'écrit d'informatique (Polytechnique et ENS, 2015), pour PSI et PT.
    Ce sujet d'écrit a été posé en devoir écrit surveillé (DS) pour le cours d'informatique pour tous en prépa MP au Lycée Lakanal.
    
    
    - *Date :* Vendredi 11 décembre 2015,
    - *Auteur :* Lilian Besson, pour le cours d'informatique pour tous en prépa MP (http://perso.crans.org/besson/infoMP/),
    - *Licence :* MIT Licence (http://lbesson.mit-license.org).
    
    
    .. note::
    
       Cette correction ne contient aucune documentation, mais elle est plus consise.
    
       - Voir `le code de cette correction <_modules/X_PSI_PT__2015__minimal_solution.html>`_.
       - Voir `l'autre correction <X_PSI_PT__2015.html>`_ pour plus d'exemples et de détails.

FUNCTIONS
    affiche(*args)
    
    afficheToutesLesRoutes(plan)
        # %% Question 8
    
    ajouteDansListe(liste, x)
        # %% Question 3
    
    ajouteRoute(plan, x, y)
        # %% Question 7
    
    coloriageAleatoire(plan, couleur, k, s, t)
        # %% Question 9
    
    creerListeVide(n)
        # %% Question 1
    
    creerPlanSansRoute(n)
        # %% Question 5
    
    creerTableau(n)
    
    creerTableau2D(p, q)
    
    entierAleatoire(k)
    
    estDansListe(liste, x)
        # %% Question 2
    
    estVoisine(plan, x, y)
        # %% Question 6
    
    existeCheminArcEnCiel(plan, couleur, k, s, t)
        # %% Question 12
    
    existeCheminSimple(plan, k, s, t)
        # %% Question 13
    
    voisinesDeCouleur(plan, couleur, i, c)
        # %% Question 10
    
    voisinesDeLaListeDeCouleur(plan, couleur, liste, c)
        # %% Question 11

DATA
    print_function = _Feature((2, 6, 0, 'alpha', 2), (3, 0, 0, 'alpha', 0)...


