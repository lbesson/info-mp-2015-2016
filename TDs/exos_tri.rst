`Exercices sur les algorithmes de tris <../exos_tri.pdf>`_
==========================================================
6ème et dernier TD, sur les algorithmes de tris.

.. automodule:: exos_tri
    :members:
    :undoc-members:

--------------------------------------------------------------------------------

Sortie du script
----------------
.. runblock:: console

   $ python3 exos_tri.py

Le fichier Python se trouve ici : :download:`exos_tri.py`.
