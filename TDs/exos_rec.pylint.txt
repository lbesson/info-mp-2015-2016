************* Module exos_rec
C: 31, 0: No space allowed after bracket
for (a, b) in [ (10, 500), (2, 6000), (19, 93) ]:
              ^ (bad-whitespace)
C: 31, 0: No space allowed before bracket
for (a, b) in [ (10, 500), (2, 6000), (19, 93) ]:
                                               ^ (bad-whitespace)
W: 45, 0: Anomalous backslash in string: '\l'. String constant might be missing an r prefix. (anomalous-backslash-in-string)
W: 45, 0: Anomalous backslash in string: '\m'. String constant might be missing an r prefix. (anomalous-backslash-in-string)
W: 67, 0: Anomalous backslash in string: '\l'. String constant might be missing an r prefix. (anomalous-backslash-in-string)
W: 67, 0: Anomalous backslash in string: '\l'. String constant might be missing an r prefix. (anomalous-backslash-in-string)
W:106, 0: Anomalous backslash in string: '\l'. String constant might be missing an r prefix. (anomalous-backslash-in-string)
W:151, 0: Anomalous backslash in string: '\l'. String constant might be missing an r prefix. (anomalous-backslash-in-string)
W: 19,17: Redefining name 'a' from outer scope (line 31) (redefined-outer-name)
W: 19,20: Redefining name 'b' from outer scope (line 31) (redefined-outer-name)
W: 19,23: Redefining name 'n' from outer scope (line 33) (redefined-outer-name)
W: 25, 4: Redefining name 'u' from outer scope (line 34) (redefined-outer-name)
W: 25, 7: Redefining name 'v' from outer scope (line 34) (redefined-outer-name)
W: 26, 8: Unused variable 'i' (unused-variable)
W: 44,12: Redefining name 'a' from outer scope (line 31) (redefined-outer-name)
W: 44,15: Redefining name 'b' from outer scope (line 31) (redefined-outer-name)
W: 66,16: Redefining name 'a' from outer scope (line 31) (redefined-outer-name)
W: 66,19: Redefining name 'b' from outer scope (line 31) (redefined-outer-name)
R: 66, 0: Too many return statements (8/6) (too-many-return-statements)
W:113, 4: Redefining name 'a' from outer scope (line 31) (redefined-outer-name)
W:105, 6: Redefining name 'x' from outer scope (line 122) (redefined-outer-name)
W:105, 9: Redefining name 'n' from outer scope (line 33) (redefined-outer-name)
W:135,18: Redefining name 'n' from outer scope (line 33) (redefined-outer-name)
W:150,14: Redefining name 'n' from outer scope (line 33) (redefined-outer-name)


Report
======
89 statements analysed.

Statistics by type
------------------

+---------+-------+-----------+-----------+------------+---------+
|type     |number |old number |difference |%documented |%badname |
+=========+=======+===========+===========+============+=========+
|module   |1      |NC         |NC         |100.00      |0.00     |
+---------+-------+-----------+-----------+------------+---------+
|class    |0      |NC         |NC         |0           |0        |
+---------+-------+-----------+-----------+------------+---------+
|method   |0      |NC         |NC         |0           |0        |
+---------+-------+-----------+-----------+------------+---------+
|function |6      |NC         |NC         |100.00      |83.33    |
+---------+-------+-----------+-----------+------------+---------+



Raw metrics
-----------

+----------+-------+------+---------+-----------+
|type      |number |%     |previous |difference |
+==========+=======+======+=========+===========+
|code      |93     |58.49 |NC       |NC         |
+----------+-------+------+---------+-----------+
|docstring |38     |23.90 |NC       |NC         |
+----------+-------+------+---------+-----------+
|comment   |8      |5.03  |NC       |NC         |
+----------+-------+------+---------+-----------+
|empty     |20     |12.58 |NC       |NC         |
+----------+-------+------+---------+-----------+



Duplication
-----------

+-------------------------+------+---------+-----------+
|                         |now   |previous |difference |
+=========================+======+=========+===========+
|nb duplicated lines      |0     |NC       |NC         |
+-------------------------+------+---------+-----------+
|percent duplicated lines |0.000 |NC       |NC         |
+-------------------------+------+---------+-----------+



Messages by category
--------------------

+-----------+-------+---------+-----------+
|type       |number |previous |difference |
+===========+=======+=========+===========+
|convention |2      |NC       |NC         |
+-----------+-------+---------+-----------+
|refactor   |1      |NC       |NC         |
+-----------+-------+---------+-----------+
|warning    |21     |NC       |NC         |
+-----------+-------+---------+-----------+
|error      |0      |NC       |NC         |
+-----------+-------+---------+-----------+



Messages
--------

+------------------------------+------------+
|message id                    |occurrences |
+==============================+============+
|redefined-outer-name          |14          |
+------------------------------+------------+
|anomalous-backslash-in-string |6           |
+------------------------------+------------+
|bad-whitespace                |2           |
+------------------------------+------------+
|unused-variable               |1           |
+------------------------------+------------+
|too-many-return-statements    |1           |
+------------------------------+------------+



Global evaluation
-----------------
Your code has been rated at 7.30/10
If you commit now, people should not be making nasty comments about you on c.l.py

