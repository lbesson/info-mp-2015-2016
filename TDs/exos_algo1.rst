`Exercices d'algorithmique, 1 <../exos_algo1.pdf>`_
===================================================
1er TD, quelques exercices d'algorithmique de base.

.. automodule:: exos_algo1
    :members:
    :undoc-members:

--------------------------------------------------------------------------------

Sortie du script
----------------
.. runblock:: console

   $ python3 exos_algo1.py

Le fichier Python se trouve ici : :download:`exos_algo1.py`.
