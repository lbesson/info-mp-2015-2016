`Exercices sur les piles et les files <../exos_piles_files.pdf>`_
=================================================================
3ème TD, sur les piles et les files.

Pour l'implémentation d'une file avec deux piles, j'utilise une approche objet pour enrober ça joliment, mais le code se comprendra quand même.
Il y a deux approches possibles, chacune lente et rapide sur une des opérations ``push`` ou ``pop``.

.. automodule:: exos_piles_files
    :members:
    :undoc-members:
    :show-inheritance:

--------------------------------------------------------------------------------

Sortie du script
----------------
.. runblock:: console

   $ python3 exos_piles_files.py

Le fichier Python se trouve ici : :download:`exos_piles_files.py`.
