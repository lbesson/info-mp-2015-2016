#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
TD1 au Lycée Lakanal (sujet rédigé par Arnaud Basson).

- *Date :* jeudi 24 septembre 2015,
- *Auteur :* Lilian Besson, pour le cours d'informatique pour tous en prépa MP (http://perso.crans.org/besson/infoMP/),
- *Licence :* MIT Licence (http://lbesson.mit-license.org).
"""


from __future__ import print_function  # Python 2 compatibility


## Exercice 1 : Nombres parfaits
def sommeDiviseurs(n):
    """ Calcule la somme des diviseurs positifs de n."""
    s = 0
    for i in range(1, n+1):
        if n % i == 0:  # i | n
            s = s + i  # s += i
    return s
    # return sum(i for i in range(1, n+1) if n % i == 0)


def estParfait(n):
    r""" Vérifie que :math:`2n = \sum_{d|n} d`."""
    return 2*n == sommeDiviseurs(n)


def prochainParfait(n):
    """ Calcul le prochain nombre parfait."""
    i = n
    while not estParfait(i):
        i += 1
    return i


## Exercice 2 : Nombre d'occurences du maximum
def nombreMaximum(t):
    """ Calcule le nombre maximum d'occurences du maximum."""
    nb = 0
    maximumActuel = t[0]
    for valeur in t:
        if valeur == maximumActuel:
            nb += 1
        elif valeur > maximumActuel:
            nb = 1
            maximumActuel = valeur
    return nb


## Exercice 3 : Permutation
def estPermutation(t):
    """ Vérifie si t est une permutation."""
    n = len(t)
    aux = [-1] * n
    for i in range(n):
        aux[t[i]] = i
    return all(x >= 0 for x in aux)

def reciproque(t):
    """ Calcule la permutation réciproque."""
    assert estPermutation(t)
    aux = [-1] * len(t)
    for i in range(len(t)):
        aux[t[i]] = i
    return aux


## Exercice 4 : Defi mathematique
s = lambda n: sum(int(c)**5 for c in str(n))

# Juste une borne, par dichotomie logarithmique "a la main"
nMax = 300000
# http://www.wolframalpha.com/input/?i=plot%28x+%2F+floor%28log%2810%2C+x%29%29+-+9**5%2C+x%3D295200%2C+x%3D295300%29

print("On cherche les nombres n = s(n) entre 0 et {}.".format(nMax))
nombre = 0
for i in range(nMax):
    if i == s(i):
        print("Le nombre {} est egal a la somme de ses chiffres eleves a la puissance 5".format(i))
        nombre += 1

print("Inutile de chercher au dela de {nMax} puisque la fonction n > floor(log(n, 10)) * 9**5 >= s(n) pour n > {nMax}.".format(nMax=nMax))

print("Il y a {} nombres entiers a etre egaux a la somme de leurs chiffres eleves a la puissance 5.".format(nombre))

# nombre = sum(1 for i in range(nMax) if i == s(i))
