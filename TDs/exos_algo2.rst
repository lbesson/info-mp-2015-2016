`Exercices d'algorithmique, 2 <../exos_algo2.pdf>`_
===================================================
2ème TD, quelques exercices d'algorithmique de base.

.. automodule:: exos_algo2
    :members:
    :undoc-members:

--------------------------------------------------------------------------------

Sortie du script
----------------
.. runblock:: console

   $ python3 exos_algo2.py

Le fichier Python se trouve ici : :download:`exos_algo2.py`.
