`Exercices sur les méthodes numériques <../exos_num.pdf>`_
==========================================================
5ème TD, sur les méthodes numériques.

.. automodule:: exos_num
    :members:
    :undoc-members:

--------------------------------------------------------------------------------

Sortie du script
----------------
.. runblock:: console

   $ python3 exos_num.py

Le fichier Python se trouve ici : :download:`exos_num.py`.
