TD Informatique pour tous - Prépa MP Lakanal
============================================
Ci dessous se trouvent les corrections des différents Travaux Dirigés
(des liens rapides se trouvent aussi dans la *barre latérale*).

Ces Travaux Dirigés concernent l'enseignement d'informatique *« pour tous »* donné en classe de MP au Lycée Lakanal (en 2015-16).

-----------------------------------------------------------------------------

.. note:: Ces corrections demandent les modules `numpy <http://www.numpy.org/>`_ et `matplotlib <http://www.matplotlib.org/>`_.

   - Gratuitement, vous pouvez télécharger et installer la distribution `Pyzo <http://www.pyzo.org/downloads.html>`_ pour tester ces solutions sur votre ordinateur personnel.
   - Les programmes sont en `Python 3 <https://docs.python.org/3/>`_, mais devraient être valide pour `Python 2 <https://docs.python.org/2/>`_ si besoin.
   - (Plus de détails sur l'`installation de Python <http://perso.crans.org/besson/apprendre-python.fr.html>`_ si besoin.)

.. warning:: Ces solutions sont en accès libre, mais pas `les sujets <http://psi.lakanal.perso.sfr.fr/eleves/info/index.html>`_.

.. seealso:: `Solutions aux TP <../../TPs/solutions/>`_ ou `solutions aux DS <../../DSs/solutions/>`_.

-----------------------------------------------------------------------------

`TD1 : Exercices d'algorithmiques, 1 <../exos_algo1.pdf>`_
----------------------------------------------------------
- *Date :* jeudi 24 septembre 2015.
- Voir la page correspondante : `<exos_algo1.html>`_.

-----------------------------------------------------------------------------

`TD2 : Exercices d'algorithmiques, 2 <../exos_algo2.pdf>`_
----------------------------------------------------------
- *Date :* jeudi 08 octobre 2015.
- Voir la page correspondante : `<exos_algo2.html>`_.

-----------------------------------------------------------------------------

`TD3 : Exercices sur les piles et les files <../exos_piles_files.pdf>`_
------------------------------------------------------------------------
- *Date :* jeudi 22 octobre 2015.
- Voir la page correspondante : `<exos_piles_files.html>`_.

-----------------------------------------------------------------------------

`TD4 : Exercices sur la récursivité <../exos_rec.pdf>`_
-------------------------------------------------------
- *Date :* jeudi 12 novembre 2015.
- Voir la page correspondante : `<exos_rec.html>`_.

-----------------------------------------------------------------------------


`TD5 : Exercices sur les méthodes numériques <../exos_num.pdf>`_
----------------------------------------------------------------
- *Date :* jeudi 7 janvier **2016**.
- Voir la page correspondante : `<exos_num.html>`_.

-----------------------------------------------------------------------------

`TD6 : Algorithmes de tris <../exos_tri.pdf>`_
----------------------------------------------
- *Date :* jeudi 31 mars 2016 (dernier TD).
- Voir la page correspondante : `<exos_tri.html>`_.

-----------------------------------------------------------------------------

Table des matières
==================
* :ref:`genindex`,
* :ref:`modindex`,
* Nouveau : :ref:`classindex`,
* Nouveau : :ref:`funcindex`,
* Nouveau : :ref:`dataindex`,
* Nouveau : :ref:`excindex`,
* Nouveau : :ref:`methindex`,
* Nouveau : :ref:`classmethindex`,
* Nouveau : :ref:`staticmethindex`,
* Nouveau : :ref:`attrindex`,
* :ref:`search`.

.. toctree::
   :maxdepth: 4

   exos_algo1
   exos_algo2
   exos_piles_files
   exos_rec
   exos_num
   exos_tri

-----------------------------------------------------------------------------

Remarques
=========
- J'essaie de suivre les conventions de style et de syntaxe que les examinateurs des concours suivront et attendront lors des écrits et des oraux de concours.
- J'essaie de ne pas utiliser de notions, d'éléments de la syntaxe Python ni de modules hors-programme.
- Une exception à cette règle est le mot clé Python `assert <https://docs.python.org/3/reference/simple_stmts.html#assert>`_, pas très dur à comprendre mais bien pratique (il permet simplement de vérifier une assertion, par exemple ``assert n >= 0`` s'assure que l'entier ``n`` soit positif avant de continuer, et la fonction ou le programme échouera sinon).


.. hint:: `Me contacter <http://perso.crans.org/besson/callme.fr.html>`_ si besoin ?

   S'il vous plait, n'hésitez pas à me `contacter <http://perso.crans.org/besson/contact>`_
   si besoin, pour signaler une erreur, poser une question ou demander plus de détails sur une correction.
   Par courriel à ``besson à crans point org``
   (et `plus de moyens de me contacter sont sur cette page <http://perso.crans.org/besson/callme.fr.html>`_).

-----------------------------------------------------------------------------

Auteurs et copyrights
=====================
- Les sujets sont rédigés par Arnaud Basson (professeur en PSI\* et MP au Lycée Lakanal),
- Ces sujets concernent l'année scolaire 2015-16 (septembre 2015 - juin 2016),
- Ces solutions et la documentation est faite par `Lilian Besson <http://perso.crans.org/besson/>`_, et mise en ligne sous les termes de la `licence MIT <http://lbesson.mit-license.org/>`_,
- Ces sujets et ces solutions suivent le programme officiel d'« informatique pour tous » en prépa MP, `version 2013 <http://www.education.gouv.fr/pid25535/bulletin_officiel.html?cid_bo=71586>`_,
- Ces pages sont générées par `Sphinx <http://sphinx-doc.org/>`_, l'outil de documentation pour Python.
