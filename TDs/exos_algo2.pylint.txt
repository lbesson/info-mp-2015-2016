************* Module exos_algo2
C: 80, 0: Unnecessary parens after u'not' keyword (superfluous-parens)
C: 86, 0: Unnecessary parens after u'not' keyword (superfluous-parens)
C: 95, 0: No space allowed after bracket
    t = [ [0]*(n+1) for i in range(n+1) ]
        ^ (bad-whitespace)
C: 95, 0: No space allowed before bracket
    t = [ [0]*(n+1) for i in range(n+1) ]
                                        ^ (bad-whitespace)
C:115, 0: No space allowed before bracket
        t[1:m+1] = [t[p] + t[p-1] for p in range(1, m+1) ]
                                                         ^ (bad-whitespace)
C:157, 0: Exactly one space required after comma
                                    print("Une autre facon est : " + str((i1,i2,i3,i4,i5,i6)))
                                                                            ^ (bad-whitespace)
C:157, 0: Exactly one space required after comma
                                    print("Une autre facon est : " + str((i1,i2,i3,i4,i5,i6)))
                                                                               ^ (bad-whitespace)
C:157, 0: Exactly one space required after comma
                                    print("Une autre facon est : " + str((i1,i2,i3,i4,i5,i6)))
                                                                                  ^ (bad-whitespace)
C:157, 0: Exactly one space required after comma
                                    print("Une autre facon est : " + str((i1,i2,i3,i4,i5,i6)))
                                                                                     ^ (bad-whitespace)
C:157, 0: Exactly one space required after comma
                                    print("Une autre facon est : " + str((i1,i2,i3,i4,i5,i6)))
                                                                                        ^ (bad-whitespace)
W: 16,14: Redefining name 'n' from outer scope (line 106) (redefined-outer-name)
W: 29,15: Redefining name 'n' from outer scope (line 106) (redefined-outer-name)
C: 29, 0: Missing function docstring (missing-docstring)
W: 33,20: Redefining name 'n' from outer scope (line 106) (redefined-outer-name)
C: 33, 0: Missing function docstring (missing-docstring)
W: 44, 4: Redefining name 'n' from outer scope (line 106) (redefined-outer-name)
C: 43, 0: Missing function docstring (missing-docstring)
W: 63, 4: Redefining name 'n' from outer scope (line 106) (redefined-outer-name)
C: 60, 0: Missing function docstring (missing-docstring)
C: 75, 0: Missing function docstring (missing-docstring)
W: 93,19: Redefining name 'n' from outer scope (line 106) (redefined-outer-name)
W:110,20: Redefining name 'n' from outer scope (line 106) (redefined-outer-name)
W:145, 0: Dangerous default value pieces (__builtin__.list) as argument (dangerous-default-value)


Report
======
105 statements analysed.

Statistics by type
------------------

+---------+-------+-----------+-----------+------------+---------+
|type     |number |old number |difference |%documented |%badname |
+=========+=======+===========+===========+============+=========+
|module   |1      |NC         |NC         |100.00      |0.00     |
+---------+-------+-----------+-----------+------------+---------+
|class    |0      |NC         |NC         |0           |0        |
+---------+-------+-----------+-----------+------------+---------+
|method   |0      |NC         |NC         |0           |0        |
+---------+-------+-----------+-----------+------------+---------+
|function |11     |NC         |NC         |54.55       |100.00   |
+---------+-------+-----------+-----------+------------+---------+



Raw metrics
-----------

+----------+-------+------+---------+-----------+
|type      |number |%     |previous |difference |
+==========+=======+======+=========+===========+
|code      |106    |70.20 |NC       |NC         |
+----------+-------+------+---------+-----------+
|docstring |13     |8.61  |NC       |NC         |
+----------+-------+------+---------+-----------+
|comment   |10     |6.62  |NC       |NC         |
+----------+-------+------+---------+-----------+
|empty     |22     |14.57 |NC       |NC         |
+----------+-------+------+---------+-----------+



Duplication
-----------

+-------------------------+------+---------+-----------+
|                         |now   |previous |difference |
+=========================+======+=========+===========+
|nb duplicated lines      |0     |NC       |NC         |
+-------------------------+------+---------+-----------+
|percent duplicated lines |0.000 |NC       |NC         |
+-------------------------+------+---------+-----------+



Messages by category
--------------------

+-----------+-------+---------+-----------+
|type       |number |previous |difference |
+===========+=======+=========+===========+
|convention |15     |NC       |NC         |
+-----------+-------+---------+-----------+
|refactor   |0      |NC       |NC         |
+-----------+-------+---------+-----------+
|warning    |8      |NC       |NC         |
+-----------+-------+---------+-----------+
|error      |0      |NC       |NC         |
+-----------+-------+---------+-----------+



Messages
--------

+------------------------+------------+
|message id              |occurrences |
+========================+============+
|bad-whitespace          |8           |
+------------------------+------------+
|redefined-outer-name    |7           |
+------------------------+------------+
|missing-docstring       |5           |
+------------------------+------------+
|superfluous-parens      |2           |
+------------------------+------------+
|dangerous-default-value |1           |
+------------------------+------------+



Global evaluation
-----------------
Your code has been rated at 7.81/10
If you commit now, people should not be making nasty comments about you on c.l.py

