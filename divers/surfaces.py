#!/usr/bin/env python3
# -*- coding: utf-8; mode: python -*-
""" Small demonstration script to draw surfaces with Python
"""

from __future__ import print_function  # Python 2/3 compatibility!

import numpy as np
import matplotlib.pyplot as plt
from matplotlib import cm
# from matplotlib.ticker import LinearLocator, FormatStrFormatter
from mpl_toolkits.mplot3d import Axes3D


# %% Exemple du cours

if __name__ == '__main__':
    fig = plt.figure()
    ax = fig.gca(projection='3d')
    X = np.arange(-3, 3, 0.1)
    Y = np.arange(0.1, 5, 0.08)
    X, Y = np.meshgrid(X, Y)
    Z = (X**2 - np.log(Y)**2)

    surf = ax.plot_surface(X, Y, Z, rstride=1, cstride=2, cmap=cm.coolwarm, linewidth=0.4, antialiased=False)

    niv = [-3, -2, -1.6, -1.3] + list(np.arange(-1, .3, 0.2)) + [.5, 1, 2, 4, 7]

    ax.contour(X, Y, Z, zdir='z', levels=niv, linewidths=1.5, offset=-6)
    ax.set_zlim(-6, 6)

    plt.show()
    # DONE


# %% Exercice n°15
# extrema de f(x,y) = y * (x**2 + ln(y)**2)

if __name__ == '__main__':
    fig = plt.figure()
    ax = fig.gca(projection='3d')

    X = np.arange(-2, 2, 0.1)
    Y = np.arange(1e-10, 2.5, 0.03)
    X, Y = np.meshgrid(X, Y)
    Z = Y * (X**2 + np.log(Y)**2)

    surf = ax.plot_surface(X, Y, Z, rstride=3, cstride=1, cmap=cm.coolwarm, linewidth=0.1, antialiased=False)

    niv2 = list(np.arange(.05, .5, .1)) + [.53995, .6] + list(np.arange(.8, 4, .5))

    ax.contour(X, Y, Z, zdir='z', levels=niv2, linewidths=1.5, offset=-1)
    ax.set_zlim(-1, 6)

    plt.show()
    # DONE
