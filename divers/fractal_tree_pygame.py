#!/usr/bin/env python3
# -*- coding: utf-8; mode: python -*-
""" Generate and draw a fractal tree.

To draw a fractal tree is simple:

1. Draw the trunk
2. At the end of the trunk, split by some angle and draw two branches
3. Repeat at the end of each branch until a sufficient level of branching is reached
"""

from __future__ import print_function  # Python 2/3 compatibility!

import pygame
import math

if __name__ == '__main__':
    pygame.init()
    window = pygame.display.set_mode((600, 600))
    pygame.display.set_caption("Fractal Tree")
    screen = pygame.display.get_surface()


def drawTree(x1, y1, angle, depth):
    """ Recursively draw the tree."""
    if depth:
        x2 = x1 + int(math.cos(math.radians(angle)) * depth * 10.0)
        y2 = y1 + int(math.sin(math.radians(angle)) * depth * 10.0)
        pygame.draw.line(screen, (255, 255, 255), (x1, y1), (x2, y2), 2)
        drawTree(x2, y2, angle - 20, depth - 1)
        drawTree(x2, y2, angle + 20, depth - 1)


def shouldIQuit(event):
    """ Check if the event should make the game quit (nicely with exit(0))."""
    if event.type == pygame.QUIT:
        exit(0)


if __name__ == '__main__':
    drawTree(300, 550, -90, 9)
    pygame.display.flip()
    while True:
        shouldIQuit(pygame.event.wait())
