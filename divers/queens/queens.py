#! /usr/bin/env python3
# -*- coding: utf-8; mode: python -*-
""" Solve the n-Queen problem with a trick thanks to permutation."""
from sys import argv
from itertools import permutations


def board(vec, n):
    print (" " + "_"*(n*2-1) + "\n" + "\n".join("|" + "_|" * i + "Q" + "|_" * (n-i-1) + "|" for i in vec) + "\n\n" + ("~"*(n*2)) + "\n")

n = int(argv[1]) if len(argv) > 1 else 8
cols = range(n)
nbsol = 0
for vec in permutations(cols):
    # This is memory efficient, it's a generator and not a list
    if n == len(set(vec[i]+i for i in cols)) \
         == len(set(vec[i]-i for i in cols)):
        board(vec, n)
        nbsol += 1

print("\n==> For {n} queens, there is {nbsol} solutions (displayed above).".format(n=n, nbsol=nbsol))
