Help on module fractal_tree:

NAME
    fractal_tree - Mandelbrot and Frame's (Binary) Fractal Trees.

FILE
    /home/lilian/teach/info-mp-2015-2016/divers/fractal_tree.py

DESCRIPTION
    See http://www.math.union.edu/research/fractaltrees/
    https://code.activestate.com/recipes/347736-mandelbrotframe-binary-fractal-trees/

FUNCTIONS
    cos(...)
        cos(x)
        
        Return the cosine of x (measured in radians).
    
    ftree(iternum, origin, t, r, theta, dtheta)
        Extend the fractal tree one iteration.
        
        - iternum:     The iteration number (we stop when iternum == 0)
        - origin:   The x, y coordinates of the start of this branch
        - t:        The current trunk length
        - r:        The amount to contract the trunk each iteration
        - theta:    The current orientation
        - dtheta:   The angle of the branch
    
    main()
        Main function. Here we choose initial values.
        These work fairly well. See ftree for details.
    
    pil_render_lines(lines, height=300, width=300, fname='bs.png')
        Render the lines on a new PIL image.
    
    sin(...)
        sin(x)
        
        Return the sine of x (measured in radians).

DATA
    pi = 3.141592653589793
    print_function = _Feature((2, 6, 0, 'alpha', 2), (3, 0, 0, 'alpha', 0)...


